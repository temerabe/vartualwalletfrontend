import { Component, HostListener} from '@angular/core';
import { BnNgIdleService } from 'bn-ng-idle';
import { AuthenticationService } from './authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // @HostListener("window:beforeunload",["$event"])
  // clearLocalStorage(event){
  //   this.auth.logOut();
  //     localStorage.clear();
  // }
  constructor(private auth: AuthenticationService) {
  }
}
