import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Transaction } from 'app/customers/Transaction';
import { Customer } from 'app/customers/Customer';
import { UserserviceService } from 'app/userservices/userservice.service';
import { first } from 'rxjs/operators';
import { AppSettings } from 'app/app-settings';
import { Memoize } from 'lodash-decorators';
import { BnNgIdleService } from 'bn-ng-idle';
import { AuthenticationService } from 'app/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-employee-dashboard',
  templateUrl: './employee-dashboard.component.html',
  styleUrls: ['./employee-dashboard.component.scss']
})
export class EmployeeDashboardComponent implements OnInit {
  message: string = '';
  isMsg = false;
  display = false;
  transaction: Transaction = new Transaction();
  customer: Customer = new Customer();
  image = './assets/img/faces/default.png';
  delegImage = './assets/img/faces/default.png';
  constructor(
    private userService: UserserviceService,
    private bnIdle: BnNgIdleService,
    private auth: AuthenticationService,
    private router: Router
  ) { 
    this.bnIdle.startWatching(900).subscribe((res) => {
      if(res) {
          console.log("session expired");
          this.auth.logOut();
          this.router.navigate(['/login']);
      }
    })
  }

  ngOnInit() {
  }
  onSubmit(f: NgForm) {
    if (f.value.code === '' || f.value.code === null) {
      this.message = 'قم بإدخال رقم العملية';
      this.isMsg = true;
    }
    else {
      this.getTransaction(f.value.code);
    }
  }
  close() {
    this.isMsg = false;
  }
  execute(trNo: number) {
    if (confirm('هل أنت متأكد من أنك تريد إكمال العملية ؟')) {
      this.userService.executeTransaction(trNo).pipe(first()).subscribe(
        msg => {
          this.message = msg;
          this.isMsg = true;
          this.display = false;
        },
        (error) => {
          console.log('server error')
          this.message = 'الرقم غير صحيح';
          this.isMsg = true;
        },
        () => { console.log('completed...') }
      );
    }
  }
  getCustomer(accountno: number) {
    this.userService.getCustomer(accountno).pipe(first()).subscribe(
      (cust: Customer) => {
        this.customer = cust;
      },
      (error) => { console.log('server errir') },
      () => { console.log('completed...') }
    );
  }
  getTransaction(trno: number) {
    let transaction: Transaction;
    this.userService.getCompleteTransaction(trno).pipe(first()).subscribe(
      (tx: Transaction) => {
        this.transaction = tx;
        if (tx === null || tx === undefined) {
          this.message = 'الرقم غير صحيح أو غير موجود';
          this.isMsg = true;
        }
        else {
          this.display = true;
          //this.image = AppSettings.APP_ENDPOINT + '/customer/image/' + tx.custId;
          this.image= tx.custImage;
          //this.delegImage = AppSettings.APP_ENDPOINT + '/customer/delegImage/' + tx.custId;
          this.delegImage = tx.custDelegImage;
        }
      },
      (error) => { 
        this.message = 'فشل الإتصال بالسيرفر ، الرجا المحاولة مرة أخرى';
        this.isMsg = true;
        console.log('server error') },
      () => { console.log('completed...') }
    );
  }
  @Memoize
  getTxStatus(value){
    if(value.trType === 'deposit'){
      return "إيداع";
    }
    else{
      return "سحب";
    }
  }
}