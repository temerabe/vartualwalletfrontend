import { PipeTransform, Pipe } from "@angular/core";
import { Customer } from "app/customers/Customer";
@Pipe({
    name: 'customerFilter'
})
export class CustomerFilterPipe implements PipeTransform{
    transform(emps: Customer[],searchTerm: string) : Customer[]{
        if(!emps || !searchTerm){
            return emps;
        }
        return emps.filter(employee => employee.firstName.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1
        );
    }
}