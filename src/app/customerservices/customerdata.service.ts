import { Injectable } from '@angular/core';
import { Customer } from 'app/customers/Customer';
import { CustomersService } from 'app/customers/customers.service';
import { WithdrawRequest } from 'app/customers/WithdrawRequest';

@Injectable({
  providedIn: 'root'
})
export class CustomerdataService {
  
  constructor(private custServiec : CustomersService) { }
  getWithdraws(accountNo : number) : WithdrawRequest[] {
    let withdraw : any = [];
    this.custServiec.getWithdraws(accountNo).subscribe(
      (cust : WithdrawRequest[]) => {
        if(cust){
          withdraw = cust;
        }
        else{
          withdraw = null;
        }
      },
      (err) => {console.log('customerdataservice -> getWithdraws -> server error')},
      () => {console.log('customerdataservice -> getWithdraws -> completed...')}
    );
    return withdraw;
  }
}
