import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomersService } from '../customers.service';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-customer-forgot-password',
  templateUrl: './customer-forgot-password.component.html',
  styleUrls: ['./customer-forgot-password.component.scss']
})
export class CustomerForgotPasswordComponent implements OnInit {
  isExistEmail : boolean = false;
  message : string = 'البريد الإلكتروني غير موجود';
  constructor(
    private router : Router,
    private route : ActivatedRoute,
    private custService : CustomersService
    ) { }

  ngOnInit() {
  }
  onSubmit(form : NgForm){
    this.custService.forgotPassword(form.value.email).pipe(first()).subscribe(
      (res) => {
        if(res === 'تم إرسال رابط تعيين كلمة المرور لبريدك الإلكتروني'){
          this.router.navigate(['/login']);
        }
        else{
          this.message = res;
          this.isExistEmail = true;
        }
      },
      (error) => {
        this.message = 'فشل الإتصال يالسيرفر ، حاول مرة أخرى';
        this.isExistEmail = true;
      },
      () => {
        console.log('completed...');
      }
    );
  }

  back(){
    this.router.navigate(['/login']);
  }


}
