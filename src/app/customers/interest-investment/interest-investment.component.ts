import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../customers.service';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Customer } from '../Customer';
import { BnNgIdleService } from 'bn-ng-idle';

@Component({
  selector: 'app-interest-investment',
  templateUrl: './interest-investment.component.html',
  styleUrls: ['./interest-investment.component.scss']
})
export class InterestInvestmentComponent implements OnInit {
  msg : string;
  isMsg : boolean = false;
  customer : Customer = new Customer();
  constructor(private custService : CustomersService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService
    ) {
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
     }
  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
      const cust = JSON.parse(localStorage.getItem('user'));
      this.custService.getCustomer(cust.id).pipe(first()).subscribe(
        (customer) => {
          this.customer = customer;
        },
        (error) => {console.log('server error')},
        () => {console.log('completed...')}
      );
    }
    else{
      this.router.navigate(['/login']);
    }
  }
  onSubmit(form){
    if(form.value.amount === null || form.value.amount === undefined || form.value.amount === ''){
      this.msg = 'الرجاء إدخال المبلغ المراد تشغيله';
      this.isMsg = true;
    }
    else{
      if(confirm('هل أنت متأكد من أنك تريد تشغيل هذا المبلغ ؟')){
            this.custService.investMoney(this.customer.id,form.value.amount).pipe(first()).subscribe(
              (res) => {
                this.msg = res;
                this.isMsg = true;
              },
              (err) => {
                this.msg = 'فشل الإتصال بالسيرفر';
                this.isMsg = true;
                console.log('server error')},
              () => { console.log('completed')}
            );
      }
    }
    // if(confirm('هل أنت متأكد من أنك تريد تشغيل أرباحك ؟')){
    //   this.custService.investMoney(this.customer).pipe(first()).subscribe(
    //     (res) => {
    //       this.msg = res;
    //       this.isMsg = true;
    //     },
    //     (err) => {console.log('server error')},
    //     () => { console.log('completed')}
    //   );
    // }
  }
  close(){
    this.isMsg = false;
  }
}
