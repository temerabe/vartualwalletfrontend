import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../customers.service';
import { NgForm } from '@angular/forms';
import { Customer } from '../Customer';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
declare const $:any;
@Component({
  selector: 'app-deposite-request',
  templateUrl: './deposite-request.component.html',
  styleUrls: ['./deposite-request.component.scss']
})
export class DepositeRequestComponent implements OnInit {
  customer : Customer = new Customer();
  msg : string ;
  isMsg : boolean = false;
  balance;
  constructor(private custService : CustomersService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService
    ) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }
  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
      this.customer = JSON.parse(localStorage.getItem('user'));
    }
    else{
      this.router.navigate(['/login']);
    }
  }
  onSubmit(deposit : NgForm){
    //const customer : any = localStorage.getItem('cust');
    this.custService.depositMoney({
      accountNo :  this.customer.accountNo,
	    amount : deposit.value.amount,
    	name : this.customer.firstName+ ' ' + this.customer.lastName
    }).subscribe(
      (res) => {
        this.msg =res;
        this.isMsg = true;
      },
      (error) => {
        console.log('server error ');
      },
      () => {
        console.log('completed...');
      }
    );
  }
close(){
  this.isMsg = false;
}
}
