export class TransferRequest {
    public id : number;
	public accountNo : number;
	public amount : number;
	public status : string;
	public receName : string;
	public receAccountNo : number;
	public recePassportNo : number;
	//public transferWay : string;
	public createdAt : any;
	public approvedAt : any;
	public receivedAt : any;
}