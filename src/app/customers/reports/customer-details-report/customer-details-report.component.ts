import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { CustomersService } from 'app/customers/customers.service';
import { Customer } from 'app/customers/Customer';
import { first } from 'rxjs/operators';
import { Transaction } from 'app/customers/Transaction';
import { Debounce, Memoize } from 'lodash-decorators';
import { BnNgIdleService } from 'bn-ng-idle';
@Component({
  selector: 'app-customer-details-report',
  templateUrl: './customer-details-report.component.html',
  styleUrls: ['./customer-details-report.component.scss']
})
export class CustomerDetailsReportComponent implements OnInit {
  cutomer : Customer = new Customer();
  transaction : Transaction[] = [];
  page = 1;
  pageSize = 3;
  constructor(private auth : AuthenticationService,
    private router : Router,
    private custService : CustomersService,
    private bnIdle: BnNgIdleService
    ) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }

  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
      this.cutomer = JSON.parse(localStorage.getItem('user'));
      console.log('accountNo = ' + this.cutomer.accountNo);
      this.loadTransaction();
    }
    else{
      this.router.navigate(['/login']);
    }
  }
  loadTransaction(){
    this.custService.getTransactions(this.cutomer.accountNo).pipe(first()).subscribe(
      (tx : Transaction[]) => {
        this.transaction = tx;
      },
      (res) => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  @Memoize()
  getTxType(value){
    let txType = '';
    if(value.trType === 'deposit'){
      txType ='إيداع';
    }
    else if(value.trType === 'withdraw'){
      txType = 'سحب'
    } else if(value.trType === 'canceled'){
      txType = 'ملغية';
    }
    else{
      txType = 'تحويل';
    }
    return txType;
}
}