import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'app/authentication.service';
import { CustomersService } from 'app/customers/customers.service';
import { CustomerdataService } from 'app/customerservices/customerdata.service';
import { Router } from '@angular/router';
import { Deposit } from 'app/customers/Deposit';
import { first } from 'rxjs/operators';
import { Memoize } from 'lodash-decorators';
import { BnNgIdleService } from 'bn-ng-idle';

@Component({
  selector: 'app-customer-deposit-report',
  templateUrl: './customer-deposit-report.component.html',
  styleUrls: ['./customer-deposit-report.component.scss']
})
export class CustomerDepositReportComponent implements OnInit {
  isMsg : boolean = false;
  msg : string = '';
  deposits : Deposit[] =[];
  accountNo;
  page = 1;
  pageSize = 3;
  constructor(
    private auth : AuthenticationService ,
     private custService : CustomersService,
     private db : CustomerdataService,
     private router : Router,
     private bnIdle: BnNgIdleService
  ) { 
    this.bnIdle.startWatching(1800).subscribe((res) => {
      if(res) {
          console.log("session expired");
          this.auth.logOut();
          this.router.navigate(['/login']);
      }
    })
  }

  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
      const cust : any = JSON.parse(localStorage.getItem('user'));
      this.custService.getDeposits(cust.accountNo).pipe(first()).subscribe(
        dp => {this.deposits = dp}
        );
    }
    else{
      this.router.navigate(['/login']);
    }
  }
  @Memoize
  getTxStatus(value){
    console.log('the status is = ' +value.status);
    let txStatus = '';
    if(value.status === 'pendding'){
      txStatus = 'إنتظار';
    }
    else if(value.status === 'reviewed'){
      txStatus = 'تمت المراحغة';
    }
    else if(value.status === 'canceled' ){
      txStatus = 'ملغية';
    }
    else {
      txStatus = 'تم السحب';
    }
    return txStatus;
  }
  cancelTransaction(id : number){
    if(confirm('هل أنت متأكد من أنك تريد إلغاء العملية ؟')){
      this.custService.cancelTransaction(id).pipe(first()).subscribe(
        (res) => {
          this.msg = res;
          this.isMsg = true;
        },
        (error) => {
          this.msg = 'فشل الإتصال بالسيرفر ، حاول مرة أخرى';
          this.isMsg = true;
        },
        () => {console.log('completed...')}
      );
    }
  }
}