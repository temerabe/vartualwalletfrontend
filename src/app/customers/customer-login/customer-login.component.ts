import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from 'app/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomersService } from '../customers.service';
import { Customer } from '../Customer';
import { first, subscribeOn } from 'rxjs/operators';
import { UserserviceService } from 'app/userservices/userservice.service';
import { User } from 'app/admin/User';
import { AdminDataService } from 'app/admin/admin-data.service';
import { noUndefined } from '@angular/compiler/src/util';
@Component({
  selector: 'app-customer-login',
  templateUrl: './customer-login.component.html',
  styleUrls: ['./customer-login.component.scss']
})
export class CustomerLoginComponent implements OnInit {
  private username : string;
  private password : string;
  error = false;
  isSaved : boolean = false;
  message = 'إسم المستخدم أو كلمة المرور غير صحيحين';
  constructor(
     private auth : AuthenticationService ,
     private router : Router, 
     private custService : CustomersService ,
     private userService : UserserviceService,
     private adminService : AdminDataService,
     private route: ActivatedRoute
     ) { }
  ngOnInit() {
    // if(this.auth.isUserLoggedIn){
    //   this.router.navigate(['/customer-dashboard']);
    // }
  }
  checkLogin(form : NgForm){
   this.custService.checkLogin(form.value.email,form.value.password).pipe(first()).subscribe(
     (cust : Customer) => { 
       if(cust.role !== null &&  cust.role !== undefined ){
         if(cust.accountStatus === 'active'){
           console.log('account status = ' + cust.accountStatus);
        localStorage.setItem('user',JSON.stringify(cust));
       this.router.navigate(['/customer-dashboard']);
         }
         else{
           this.message = 'الحساب غير مفعل';
           this.isSaved = true;
         }
       }
       else{
         this.message = 'لا يمكن الوصول للسيرفر الأن';
         this.isSaved = true;
        console.log('username or password does not exist')  
       }
     },
     (err) => {   
      this.isSaved = true; 
      console.log('server error')},
     () => {console.log('completed..')}
   );
  }
  register(){
    this.router.navigate(['/registrer']);
  }
  forgotPassword(){
    this.router.navigate(['/forgot-password']);
  }
}