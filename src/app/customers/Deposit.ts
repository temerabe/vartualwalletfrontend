export class Deposit {
    public id : number;
    public name : string;
    public accountNo : number;
    public amount : number;
    public status : string;
    public createdAt : any;
    public aprrovedAt : any;
    public receivedAt : any;
}