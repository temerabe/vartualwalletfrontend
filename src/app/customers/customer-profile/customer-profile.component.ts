import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../customers.service';
import { Customer } from '../Customer';
import {  FormGroup } from '@angular/forms';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { AppSettings } from 'app/app-settings';
import { BnNgIdleService } from 'bn-ng-idle';
declare const $:any;
@Component({
  selector: 'app-customer-profile',
  templateUrl: './customer-profile.component.html',
  styleUrls: ['./customer-profile.component.scss']
})
export class CustomerProfileComponent implements OnInit {
  image : any;
  form : FormGroup;
  selectedFile : File;
  customer : Customer = new Customer();
  msg : string;
  isMsg : boolean = false;
  constructor(private custService : CustomersService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService
    ) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }
  ngOnInit() {
    if(this.auth.isUserLoggedIn){
      this.customer = JSON.parse(localStorage.getItem('user'));
      if(this.customer.image === null ){
        this.image = './assets/img/faces/default.png';
      }
      else{
       // this.image = AppSettings.APP_ENDPOINT+'/customer/image/'+this.customer.id;
       console.log('image path = ' + this.customer.image);
        this.image = this.customer.image;
      }
    }
    else{
      this.router.navigate(['/login']);
    }
  }
  onSubmit(profile){
    // const id = +localStorage.getItem('id');
    // this.custService.createCustomeProfile({
    //   id : this.customer.id,
    //   phoneNo : this.customer.phoneNo,
    //   passportNo : this.customer.passportNo,
    //   delegPassport : this.customer.delegPassport,
    //   delegFullName : this.customer.delegFullName
    // },this.selectedFile).subscribe(
    //   (cust : Customer) =>{
    //     this.customer = cust;
    //     this.msg = 'تم حفظ التعديلات بنجاح';
    //     this.isMsg = true;
    //   },
    //   (error)=>{
    //     this.msg = 'خطأ في السيرفر';
    //     this.isMsg = true;
    //   },
    //   ()=>{
    //     console.log('completed...');
    //   }
    // )
  }
  onFileSelected(event){
    this.selectedFile = <File>event.target.files[0];
    if(event.target.files && event.target.files[0]){
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event : any) => {
        this.image = event.target.result;
      }
    }
  }
  getStringImage(image : any){
    var result = "";
    for (var i = 0; i < image.length; i++) {
      result += String.fromCharCode(parseInt(image[i], 2));
    }
    return result;
  }
close(){
  this.isMsg = false;
}
}