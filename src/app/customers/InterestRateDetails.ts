export class interestRateDeatils {
public id: number;
public firstName:string;
public middleName:string;
public lastName:string;
public accountNo:number;
public customerId:number;
public interestRate:number;
public interestRatePlus:number;
public amountWithInterestRate:number;
public amountWithInterestRatePlus:number;
public custBalance:number;
public custInteresBalance:number;
public isAdded:boolean;
public createdAt:any;
public approvedAt:any;
public intrType : string
}