export class Customer {
        public firstName: string;
        public middleName: string;
        public lastName: string;
        public password: string;
        public email: string;
        public id ?: number;
        public accountNo ? : number;
        public accountStatus ?: string;
        public image ? : any;
        public passportNo ? : number;
        public phoneNo ? : string;
        public delegFullName ? : string;
        public delegPassport ? : number;
        public delegImage ? : string;
        public totalBalance ? : number;
        public availableBalance ? : number;
        public balanceToWithdraw ? : number;
        public suspendedBalance ? : number;
        public role ? : string;
        public interestRate : number;
        public intersetRatePlus : number;
        public isProfileUpdated : boolean;
        public custPassImage: string;
}