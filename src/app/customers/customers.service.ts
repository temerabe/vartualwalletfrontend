import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from './Customer';
import { RequestOptions } from '@angular/http';
import { WithdrawRequest } from './WithdrawRequest';
import { TransferRequest } from './TransferRequest';
import { Transaction } from './Transaction';
import { AppSettings } from 'app/app-settings';
import { Form } from '@angular/forms';
import { Deposit } from './Deposit';
@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  private custUrl = AppSettings.APP_ENDPOINT+'/customers';
  constructor(private http : HttpClient) { }
  createCustomer(custromer : any) : Observable<Customer>{
    return this.http.post<Customer>(this.custUrl,custromer);
  }
  createCustomeProfile(customer : any,file : File,custPassImage: File,custDelegImage:File) : Observable<Customer> {
    const fd:FormData = new FormData();
    if(file !== undefined && file !== null ){
      fd.append('file',file,file.name);
    }
    if(custPassImage !== undefined && custPassImage !== null ){
      fd.append('custPassImage',custPassImage,custPassImage.name);
    }
    if(custDelegImage !== undefined && custDelegImage !== null ){
      fd.append('custDelegImage',custDelegImage,custDelegImage.name);
    }
    fd.append('cust',JSON.stringify(customer));
    return this.http.put<Customer>(AppSettings.APP_ENDPOINT+'/customers',fd);
  }
  getCustomer(id : number) : Observable<Customer>{
    return this.http.get<Customer>(AppSettings.APP_ENDPOINT+'/customers/'+id);
  }
  depositMoney(customer : any) {
    return this.http.put(AppSettings.APP_ENDPOINT+'/customers/deposit',customer,{responseType : 'text'});
  }
  // investMoney(Customer : any){
  //   return this.http.put(AppSettings.APP_ENDPOINT+'/customers/invest',Customer,{responseType : 'text'});
  // }
  investMoney(id : any,amount : any){
    const fd : FormData = new FormData();
    fd.append('id',id);
    fd.append('amount',amount);
    return this.http.put(AppSettings.APP_ENDPOINT+'/customers/invest',fd,{responseType : 'text'});
  }
  withdrawMoney(wm : any){
    return this.http.put(AppSettings.APP_ENDPOINT+'/customers/withdraw',wm,{responseType : 'text'});
  }
  transferMoney(tr : any){
    return this.http.put(AppSettings.APP_ENDPOINT+'/customers/transfer',tr,{responseType : 'text'});
  }
  getWithdraws(accountNo : number) : Observable<WithdrawRequest[]>{
    return this.http.get<WithdrawRequest[]>(AppSettings.APP_ENDPOINT+'/customers/withdraw/'+accountNo);
  }
  getDeposits(accountNo : number) : Observable<Deposit[]>{
    return this.http.get<Deposit[]>(AppSettings.APP_ENDPOINT+'/customers/deposit/'+accountNo);
  }
  getTransfers(accountNo : number) : Observable<TransferRequest[]>{
    return this.http.get<TransferRequest[]>(AppSettings.APP_ENDPOINT+'/customers/transfers/'+accountNo);
  }
  getTransactions(accountNo : any) : Observable<Transaction[]>{
    console.log('from customer sevice account = ' + accountNo);
    const fd : FormData = new FormData();
    fd.append('accountNo', accountNo);
    return this.http.get<Transaction[]>(AppSettings.APP_ENDPOINT+'/customers/transactions/'+accountNo);
  }
  getCustomerImage(id : number) : Observable<any[]>{
    console.log('image id = '  + id);
    return this.http.get<any[]>(AppSettings.APP_ENDPOINT+'/customer/image/'+id);
  }
  cancelTransaction(id : any ){
    const fd : FormData = new FormData();
    fd.append('id',id);
    return this.http.put(AppSettings.APP_ENDPOINT+'/customers/tx/cancel',fd,{responseType : 'text'});
  }
  checkLogin(email,password) : Observable<Customer>{
    const url = AppSettings.APP_ENDPOINT+'/customers/login';
    return this.http.post<Customer>(url,{
      email : email,
    	password : password
    });
  }
  withdrawsTx(accountno : number ) : Observable<Transaction[]>{
    return this.http.get<Transaction[]>(AppSettings.APP_ENDPOINT+'/customers/wrs/'+accountno);
  }
  resendWithdrawEmail(id : any){
    const fd : FormData = new FormData();
    fd.append('id',id);
    return this.http.post(AppSettings.APP_ENDPOINT+'/customers/tx/wr/sendmail',fd,{responseType : 'text'});
  }
  forgotPassword(email : string){
    const fd : FormData = new FormData();
    fd.append('email',email);
    return this.http.post(AppSettings.APP_ENDPOINT+'/customers/forgotpassword',fd,{responseType : 'text'});
  }
  resetPassword(reset : any){
    return this.http.post(AppSettings.APP_ENDPOINT+'/customers/resetpassword',reset,{responseType : 'text'});
  }
}