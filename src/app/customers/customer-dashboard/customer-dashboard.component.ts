import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { CustomersService } from '../customers.service';
import { Customer } from '../Customer';
import { AuthenticationService } from 'app/authentication.service';
import { first } from 'rxjs/operators';
import { Transaction } from '../Transaction';
import { BnNgIdleService } from 'bn-ng-idle';
import { MatStepper } from '@angular/material';
declare const $:any;
@Component({
  selector: 'app-customer-dashboard',
  templateUrl: './customer-dashboard.component.html',
  styleUrls: ['./customer-dashboard.component.scss']
})
export class CustomerDashboardComponent implements OnInit {
  customer : Customer = new Customer();
  transactions : Transaction[] = [];
  withdraws: Transaction[] = [];
  msg : string ;
  isMsg : boolean = false;
  isWithdraw : boolean = false;
  @ViewChild('stepper',{static:true}) stepper: MatStepper;
  isWithdrawTx : boolean = false;
  constructor(private route: ActivatedRoute,
    private custService : CustomersService,
    private router : Router,
    private auth : AuthenticationService,
    private bnIdle: BnNgIdleService
    ) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }
  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
      const cust : Customer = JSON.parse(localStorage.getItem('user'));
      console.log('the id = ' + cust.id);
      this.getCustomerProfile(cust.id);
      this.getWrTransactions(cust.accountNo);
      this.loadTransaction(cust.accountNo);
    }
    else{
      this.router.navigate(['/login']);
    }
  }
  getCustomerProfile(id){
    this.custService.getCustomer(id).pipe(first()).subscribe(
      (cust) => {
        this.customer = cust;
      },
      (error) => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  invest(){
    this.router.navigate(['/customer-interest-investment']);
    // if(confirm('هل أنت متأكد من أنك تريد تشغيل أرباحك ؟')){
    //   this.custService.investMoney(this.customer).pipe(first()).subscribe(
    //     (res) => {
    //       this.msg = res;
    //       this.isMsg = true;
    //     },
    //     (err) => {console.log('server error')},
    //     () => { console.log('completed')}
    //   );
    // }
  }
  withdraw(){
    this.router.navigate(['/customer-withdraw-request']);
    // if(confirm('هل أنت متأكد من أنك تريد سحب أرباحك ؟')){
    //   this.custService.withdrawMoney({
    //     accountNo : this.customer.accountNo,
    //     amount : this.customer.balanceToWithdraw
    //   }).pipe(first()).subscribe(
    //     (msg) => {
    //       this.msg = msg;
    //      this.isMsg = true;
    //     },
    //     (err) => {
    //       console.log('server error ');
    //     },
    //     () => {
    //       console.log('completed...');
    //     }
    //   );
    // }
  }
  loadTransaction(accountNo){
    this.custService.getTransactions(accountNo).pipe(first()).subscribe(
      (tx : Transaction[]) => {
        this.transactions = tx;
      },
      (res) => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  close(){
    this.isMsg = false;
  }
  move(index: number) {
    this.stepper.selectedIndex = index;
  }
  getWrTransactions(accountNo){
    this.custService.withdrawsTx(accountNo).pipe(first()).subscribe(
      (tx) => {
        this.withdraws = tx;
      },
      (res) => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  getIndext(t: Transaction){
    let value: number = 0;
    if(t.status === 'pendding'){
      value = 0;
    }
    else if(t.status === 'reviewed'){
      value = 1;
    }
    else{
      value = 2;
    }
    return value;
  }
}