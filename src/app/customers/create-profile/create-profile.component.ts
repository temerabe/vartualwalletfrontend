import { Component, OnInit } from '@angular/core';
import { Customer } from '../Customer';
import { CustomersService } from '../customers.service';
import { AuthenticationService } from 'app/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { BnNgIdleModule, BnNgIdleService } from 'bn-ng-idle';
@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.scss']
})
export class CreateProfileComponent implements OnInit {
  id : number;
  image: any;
  custImage : File;
  custPassportImage : File;
  custDelegeantImage : File;
  customer: Customer = new Customer();
  msg: string;
  isMsg: boolean = false;
  passImage : string = 'تحميل صورة الجواز';
  delegImage : string = 'تحميل صورة وكيل الحساب';
  isImages: boolean = true;
  isProfile: boolean = true;
  constructor(private custService: CustomersService,
    private auth: AuthenticationService,
    private router: Router,
    private route : ActivatedRoute,
    private bnIdle: BnNgIdleService
    ) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }
  ngOnInit() {
    this.image = './assets/img/faces/default.png';
    const id = +this.route.snapshot.params['id'];
    this.custService.getCustomer(id).pipe(first()).subscribe(
      (cust : Customer) => {
        this.customer = cust;
        console.log("isProfileUpdated= " + this.customer.isProfileUpdated);
        // navigate to login page if user already update his profile
        if(this.customer.isProfileUpdated){
          this.router.navigate(['/login']);
        }
      },
      () => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  onSubmit(profile) {
    this.custService.createCustomeProfile({
      id: this.customer.id,
      phoneNo: this.customer.phoneNo,
      passportNo: this.customer.passportNo,
      delegPassport: this.customer.delegPassport,
      delegFullName: this.customer.delegFullName
    }, this.custImage,this.custPassportImage,this.custDelegeantImage).subscribe(
      (cust: Customer) => {
        this.customer = cust;
        this.msg = 'تم حفظ التعديلات بنجاح ، ستتمكن من الدخول بعد موافقة مدير المنظومة ، وسيتم إخطارك عن طريق البريد الإلكتروني';
        this.isMsg = true;
        //this.isImages = false;
        this.isProfile = false;
        localStorage.setItem('user',JSON.stringify(this.customer));
       // this.router.navigate(['/customer-dashboard']);
      },
      (error) => {
        this.msg = 'خطأ في السيرفر';
        this.isMsg = true;
      },
      () => {
        console.log('completed...');
      }
    )
  }
  onCustomerImageSelected(event) {
    this.custImage = <File>event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.image = event.target.result;
      }
    }
  }
  onCustomerPassImageSelected(event) {
    this.custPassportImage = <File>event.target.files[0];
    this.passImage = this.custPassportImage.name;
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.image = event.target.result;
      }
    }
  }
  onCustomerDelegImageSelected(event) {
    this.custDelegeantImage = <File>event.target.files[0];
    this.delegImage = this.custDelegeantImage.name;
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.image = event.target.result;
      }
    }
  }
  getStringImage(image: any) {
    var result = "";
    for (var i = 0; i < image.length; i++) {
      result += String.fromCharCode(parseInt(image[i], 2));
    }
    return result;
  }
  close() {
    this.isMsg = false;
  }
}