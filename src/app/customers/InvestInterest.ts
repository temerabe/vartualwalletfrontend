export class InvestInterest{
    public id: number;
	public name: string;
	public accountNo: number;
	public amount: number;
	public status: string;
	public notes: string;
	public createdAt: any;
	public approvedAt: any;
	public receivedAt: any;
}