import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup } from '@angular/forms';
import { CustomersService } from '../customers.service';
import { Customer } from '../Customer';
import { Router } from '@angular/router';
declare const $:any;
@Component({
  selector: 'app-customer-registration',
  templateUrl: './customer-registration.component.html',
  styleUrls: ['./customer-registration.component.scss']
})
export class CustomerRegistrationComponent implements OnInit {
  registrationMsg : string;
  message : string;
  model: any = {};
  isSaved = false;
  constructor(private custService : CustomersService,private router : Router) { }
  ngOnInit() {
  }
  onSubmit(form : NgForm){
    this.custService.createCustomer({
      firstName : form.value.firstName,
      middleName : form.value.middleName,
      lastName : form.value.lastName,
      email : form.value.email,
      password : form.value.password
    }).subscribe(
      (data : Customer) => {
        this.isSaved = true;
        this.message = 'تم الحفظ بنجاح ، الرجاء تفعيل الحساب من خلال البريد الإلكتروني';
        form.reset();
      },
      (error) => {
        this.message = 'فشل بالإتصال بالسيرفر ، الرجاء مراجعة الانترنت';
        this.isSaved = true;
      },
      () => {console.log('completed...')}
    );
  }
  back(){
    this.router.navigate(['/login']);
  }
isValid(password,confirmpassword){
  console.log('password = ' + password);
  console.log('confirm = ' + confirmpassword);
  if(password === confirmpassword ){
    return true;
  }
  else{
    return false;
  }
}
}