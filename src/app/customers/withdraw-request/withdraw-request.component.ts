import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../customers.service';
import { AuthenticationService } from 'app/authentication.service';
import { Customer } from '../Customer';
import { NgForm } from '@angular/forms';
import { WithdrawRequest } from '../WithdrawRequest';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { BnNgIdleService } from 'bn-ng-idle';
declare const $:any;
@Component({
  selector: 'app-withdraw-request',
  templateUrl: './withdraw-request.component.html',
  styleUrls: ['./withdraw-request.component.scss']
})
export class WithdrawRequestComponent implements OnInit {
  msg : string;
  isMsg : boolean = false;
  customer : Customer = new Customer();
  constructor(private custService : CustomersService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService
    ) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }

  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
      const cust = JSON.parse(localStorage.getItem('user'));
      this.custService.getCustomer(cust.id).pipe(first()).subscribe(
        (customer) => {
          this.customer = customer;
        },
        (error) => {console.log('server error')},
        () => {console.log('completed...')}
      );
    }
    else{
      this.router.navigate(['/login']);
    }
  }
  onSubmit(wd : NgForm){
    console.log('amount is = ' + wd.value.amount );
    console.log('withdraw is = ' + wd.value.WithdrawWay );
    if(wd.value.amount === null || wd.value.amount === undefined || wd.value.amount === ''){
      this.msg = 'الحقول فارغة';
      this.isMsg = true;
    }
    else if(wd.value.WithdrawWay === null || wd.value.WithdrawWay === undefined || wd.value.WithdrawWay === ''){
      this.msg = 'الحقول فارغة';
      this.isMsg = true;
    }
    else{
      if(confirm('هل أنت متأكد من أنك تريد سحب مبلغ '+wd.value.amount+'؟')){
        this.custService.withdrawMoney({
          accountNo : this.customer.accountNo ,
          amount : wd.value.amount,
          withdrawWay : wd.value.WithdrawWay
        }).subscribe(
          (res) => {
            this.msg = res;
            this.isMsg = true;
          },
          (error) => {
            console.log('server error')
          },
          () => {
            console.log('completed...');
          }
        );
      }
    }
  }
close(){
  this.isMsg = false;
}
}