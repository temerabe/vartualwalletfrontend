import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomersService } from '../customers.service';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {
  private id : number;
  message : string = 'كلمات المرور غير متطابقتين';
  isSame : boolean = false ;
  constructor(
    private router : Router,
    private route : ActivatedRoute,
    private custService : CustomersService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
  }
  onSubmit(form : NgForm){
    console.log('the id is = ' + this.id);
    if(form.value.password === form.value.confirmpassword ){
      this.custService.resetPassword({
        id : this.id,
        password : form.value.password
      }).pipe(first()).subscribe(
        (res) =>{
          if(res === 'تم تغير كلمة السر بنجاح'){
            this.message = res;
            this.isSame = true;
            this.router.navigate(['/login']);
          }
          else{
            this.message = res;
            this.isSame = true;
          }
        },
        (error) => {
          this.message = 'فشل الإتصال يالسيرفر ، الرجاء المحاولة مرة أخرى';
        },
        () => {
          console.log('completed...');
        }
      );
      this.isSame = false;
    }
    else{
      this.isSame = true;
    }
  }
}