export class WithdrawRequest {
    public id : number;
	public accountNo : number;
	public amount : number;
	public createdAt : any;
	public actuAlAmount : number;
	public notes : string;
	public status : string;
    public withdrawDate : any;
    public withdrawWay : string;
}