import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../customers.service';
import { AuthenticationService } from 'app/authentication.service';
import { Customer } from '../Customer';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
declare const $:any;
@Component({
  selector: 'app-transfer-request',
  templateUrl: './transfer-request.component.html',
  styleUrls: ['./transfer-request.component.scss']
})
export class TransferRequestComponent implements OnInit {
  msg : string;
  isMsg : boolean = false;
  customer : Customer = new Customer();
  constructor(private custService : CustomersService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService
    ) {
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
     }
  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
      this.customer = JSON.parse(localStorage.getItem('user'));
    }
    else{
      this.router.navigate(['/login']);
    }
  }
  onSubmit(f:NgForm){
      if(confirm("هل أنت متأكد من أنك تريد تحويل مبلغ "+ f.value.amount)) {
        this.custService.transferMoney({
          accountNo :  this.customer.accountNo ,
          amount : f.value.amount,
          receName : f.value.receName,
          receAccountNo : f.value.receAccountNo,
          recePassportNo : f.value.recePassportNo
         // transferWay : f.value.transferWay
        }).subscribe(
          (res) => {
            this.msg = res;
            this.isMsg = true;
          },
          (error) =>{
            console.log('server error');
          },
          () => {
            console.log('completed...')
          }
        );  
      }
  }
close(){
  this.isMsg = false;
}
}
