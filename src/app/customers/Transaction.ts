export class Transaction {
    public id: number;
    public transactionNo: number;
    public amount: number;
    public custFirstName: string;
    public custMiddleName: string;
    public custLastName: string;
    public trType: string;
    public notes: string;
    public accountNo: number;
    public custPassportNo: number;
    public custDelegName: string;
    public custDelegImage: string;
    public custDelegPassportNo: number;
    public status: string;
    public createdAt: any;
    public approvedAt: any;
    public receivedAt: string;
    public custImage : string;
    public custId : number;
    public startedAt: any;
    public customerBalance: number;
}