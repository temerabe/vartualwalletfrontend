import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { AppComponent } from './app.component';
import {
  AgmCoreModule
} from '@agm/core';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HomeComponent } from './home/home.component'
import {MatButtonModule, MatCheckboxModule, MatRippleModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatTooltipModule} from '@angular/material';
import { CustomerLoginComponent } from './customers/customer-login/customer-login.component';
import { CustomerRegistrationComponent } from './customers/customer-registration/customer-registration.component';
import { CustomerForgotPasswordComponent } from './customers/customer-forgot-password/customer-forgot-password.component'
import { AdminEmployeeEditComponent } from './admin/admin-employee-edit/admin-employee-edit.component';
import { UserLoginComponent } from './user-login/user-login.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MustmatchDirective } from './customers/customer-forgot-password/mustmatch.directive';
import { CreateProfileComponent } from './customers/create-profile/create-profile.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ResetpasswordComponent } from './customers/resetpassword/resetpassword.component';
import { TestComponent } from './test/test.component';
import { BnNgIdleService } from 'bn-ng-idle';
@NgModule({
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MatPaginatorModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    }),
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MDBBootstrapModule.forRoot(),
    MatButtonModule, MatCheckboxModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    HomeComponent,
    CustomerLoginComponent,
    CustomerRegistrationComponent,
    CustomerForgotPasswordComponent,
    AdminEmployeeEditComponent,
    UserLoginComponent,
    MustmatchDirective,
    CreateProfileComponent,
    ResetpasswordComponent,
    TestComponent,
  ],
  providers: [BnNgIdleService],
  bootstrap: [AppComponent]
})
export class AppModule { }