import { Component, OnInit, Input, Output, EventEmitter, Inject, ViewChild } from '@angular/core';
import { interestRateDeatils } from 'app/customers/InterestRateDetails';
import { AdminDataService } from '../admin-data.service';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
import { first } from 'rxjs/operators';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
@Component({
  selector: 'app-admin-generated-interest',
  templateUrl: './admin-generated-interest.component.html',
  styleUrls: ['./admin-generated-interest.component.scss'],
  providers: [
    { provide: 'Window', useValue: window }
  ]
})
export class AdminGeneratedInterestComponent implements OnInit {
  @ViewChild('content', { static: false }) content;
  @Output() confirmed: EventEmitter<any> = new EventEmitter();
  @Input() interests: interestRateDeatils[];
  isDisplay: boolean = true;
  page = 1;
  pageSize = 3;
  msg1: string = 'تمت إضافة نسبة الأرباح للمستثمرين بنجاح';
  msg2: string = 'تمت إضافة نسبة الأرباح للمستثمر بنجاح';
  isMsg1: boolean = false;
  isMsg2: boolean = false;
  value: any = '';
  constructor(private adminService: AdminDataService,
    private auth: AuthenticationService,
    private router: Router,
    private bnIdle: BnNgIdleService,
    @Inject('Window') private window: Window) {
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
     }

  ngOnInit() {
    console.log("array length = " + this.interests.length);
  }
  confirmInterest(id: number) {
    if (confirm('هل تريد تأكيد إضافة نسبة الأرباج للمستثمر ؟')) {
      console.log('account no is = ' + id);
      this.adminService.confirmInterest(id).pipe(first()).subscribe(
        (res) => {
          this.confirmed.emit(null);
          this.msg2 = res;
          this.isMsg2 = true;
        },
        (error) => {
          console.log('server error')
          this.msg2 = 'فشل الإتصال بالسيرفر ، حاول مرة أخرى';
          this.isMsg2 = true;
        },
        () => { console.log('completed...') }
      );
    }
  }
  downloadPdf() {
    // let doc = new jspdf();
    // let specialElementHandlers = {
    //   '#editor': function(element,renderer){
    //     return true;
    //   }
    // };
    // let content = this.content.nativeElement;
    // doc.fromHTML(content.innerHTML,15,15,{
    //   'width': 190,
    //   'elementHandlers': specialElementHandlers
    // });

    // doc.save('test.pdf');
  }
}
