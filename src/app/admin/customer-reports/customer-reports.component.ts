import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminDataService } from '../admin-data.service';
import { first } from 'rxjs/operators';
import { InterestReport } from '../InterestReport';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-customer-reports',
  templateUrl: './customer-reports.component.html',
  styleUrls: ['./customer-reports.component.scss']
})
export class CustomerReportsComponent implements OnInit {
  currentMonthInterest: any = 0 ;
  currentYearInterest: any = 0 ;
  allInterestCount: any = 0;
  page = 1;
  pageSize = 3;
  interests: InterestReport[];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private adminService: AdminDataService,
    private datePipe: DatePipe
  ) { }
  ngOnInit() {
    const account = +this.route.snapshot.queryParamMap.get('account');
    const type = this.route.snapshot.queryParamMap.get('type');
    this.getCustomerInterest(account,type);
    this.LoadAllInterestCount(account);
    this.getMonthInterestCount(account);
  }
  getCustomerInterest(account: any,type:any){
    this.adminService.getCustomerInterest(account,type).pipe(first()).subscribe(
      (res) => {
        this.interests = res;
      },
      (error) => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  LoadAllInterestCount(account){
    var convertedDate = this.datePipe.transform(new Date(),'yyyy-MM-dd');
    this.adminService.getCustomerTotalInterestCount(account).pipe(first()).subscribe(
      (res) => {
        this.allInterestCount = res;
      },
      (error) => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  getMonthInterestCount(account){
    var convertedDate = this.datePipe.transform(new Date(),'yyyy-MM-dd');
    this.adminService.getCustomerMonthInterestCount(account).pipe(first()).subscribe(
      (res) => {
        this.currentMonthInterest= res;
      },
      (error) =>{console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  getYearInterestCount(account){
    var convertedDate = this.datePipe.transform(new Date(),'yyyy-MM-dd');
    this.adminService.getCustomerYearInterestCount(account).pipe(first()).subscribe(
      (res) => {
        this.currentYearInterest= res;
      },
      (error) =>{console.log('server error')},
      () => {console.log('completed...')}
    );
  }
}