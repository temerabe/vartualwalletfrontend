import { Component, OnInit } from '@angular/core';
import { AdminDataService } from '../admin-data.service';
import { first } from 'rxjs/operators';
import { Customer } from 'app/customers/Customer';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
declare const $ : any ;
@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  customers : Customer[] = [];
  page = 1;
  pageSize = 3;
  inActiveCustomersCount;
  penddingWithdrawCount;
  penddingTransferCount;
  penddingDepositCount;
  lastTransferCount;
  lastWithdrawCount;
  balanceToWithdrawCount;
  msg : string;
  isMsg = false;
  constructor(private adminService : AdminDataService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService
    ) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }
  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
    //this.loadPenddinRegCustomer();
    this.getInActiveCustomersCount();
    this.getPenddingWithdrawCount();
    this.getPenddingTransferCount();
    this.getPenddingDepositCount();
    this.getLastTransferCount();
    this.getlastWithdrawCount();
    this.getBalanceToWithdrawCount();
    }
    else{
      this.router.navigate(['/panel']);
    }
  }
  getInActiveCustomersCount(){
    console.log('getInActiveCustomersCount() called ... ');
    this.adminService.getInActiveCustomersCount().pipe(first()).subscribe(
      (count) => {
        this.inActiveCustomersCount = count;
        console.log('inactive customers count = ' + count);
      },
      () => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
 getPenddingWithdrawCount(){
   this.adminService.getPenddingWithdrawCount().pipe(first()).subscribe(
     (count : Number) => {
       this.penddingWithdrawCount = count;
      console.log('getPenddingWithdrawCount == ' + count);
      },
     (error) => {console.log('server error')},
     () => {console.log('completed...')}
   );
 }
 getPenddingDepositCount(){
  this.adminService.getPenddingDepositCount().pipe(first()).subscribe(
    (count : Number) => {
      this.penddingDepositCount = count;
      console.log('getPenddingDepositCount == ' + count);
    },
    (error) => {console.log('server error')},
    () => {console.log('completed...')}
  );
}
getPenddingTransferCount(){
  this.adminService.getPenddingTransferCount().pipe(first()).subscribe(
    (count : Number) => {
      this.penddingTransferCount = count;
    console.log('getPenddingTransferCount == ' + count);
    },
    (error) => {console.log('server error')},
    () => {console.log('completed...')}
  );
}
getlastWithdrawCount(){
  this.adminService.getLastWithdrawCount().pipe(first()).subscribe(
    (count : Number) => {
      this.lastWithdrawCount = count;
    console.log('getlastWithdrawCount == ' + count);
    },
    (error) => {console.log('server error')},
    () => {console.log('completed...')}
  );
}
getLastTransferCount(){
  this.adminService.getLastTransferCount().pipe(first()).subscribe(
    (count : Number) => {
      this.lastTransferCount = count;
    console.log('getLastTransferCount == ' + count);
    },
    (error) => {console.log('server error')},
    () => {console.log('completed...')}
  );
}
getBalanceToWithdrawCount(){
  this.adminService.getPenddingBalanceToToWithdraw().pipe(first()).subscribe(
    (count : Number) => {
      this.balanceToWithdrawCount = count;
    console.log('getBalanceToWithdrawCount == ' + count);
    },
    (error) => {console.log('server error')},
    () => {console.log('completed...')}
  );
}
close(){
  this.isMsg = false;
}
}