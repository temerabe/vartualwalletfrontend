import { Component, OnInit } from '@angular/core';
import { AdminDataService } from '../admin-data.service';
import { Customer } from 'app/customers/Customer';
import { User } from '../User';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
declare const $ : any;
@Component({
  selector: 'app-admin-customer',
  templateUrl: './admin-customer.component.html',
  styleUrls: ['./admin-customer.component.scss']
})
export class AdminCustomerComponent implements OnInit {
  customers : Customer[] = [];
  customer : Customer = new Customer();
  page = 1;
  pageSize = 3;
  isEdit:boolean = false;
  constructor(private adminService : AdminDataService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService
    ) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }
  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
      this.loadActiveCustomers();
    //  this.getInActiveCustomersCount();
    }
    else{
      this.router.navigate(['/panel']);
    }
  }
  loadActiveCustomers(){
    console.log('loadActiveCustomers()  called ...');
    this.adminService.getCustomers().pipe(first()).subscribe(
      ( cust : Customer[]) => {
         this.customers = cust;
       }
       );
  }
  deActivateCustomer(id : number){
    this.adminService.deActivateCustomer(id).pipe(first()).subscribe(
      msg => {
         
       }
       );
  }
  activateCustomer(id : number){
    this.adminService.activateCustomer(id).pipe(first()).subscribe(
      msg => {
         
       }
       );
  }
  edit(cust:Customer){
    this.customer = cust;
    this.isEdit = true;
  }
}