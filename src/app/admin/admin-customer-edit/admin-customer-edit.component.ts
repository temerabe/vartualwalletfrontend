import { Component, OnInit, Input } from '@angular/core';
import { Customer } from 'app/customers/Customer';
import { NgForm } from '@angular/forms';
import { AdminDataService } from '../admin-data.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-admin-customer-edit',
  templateUrl: './admin-customer-edit.component.html',
  styleUrls: ['./admin-customer-edit.component.scss']
})
export class AdminCustomerEditComponent implements OnInit {
  @Input() customer: Customer = new Customer();
  isMsg:boolean = false;
  msg:string = '';
  constructor(
    private adminService: AdminDataService
  ) { }

  ngOnInit() {
  }
  onSubmit(profile: NgForm){
    this.adminService.updateCustomer(this.customer).pipe(first()).subscribe(
      (msg) => {
        this.msg = msg;
        this.isMsg = true;
      },
      (error) => {
        console.log('server error');
      },
      () => {
        console.log('completed...');
      }
    );
  }
  close(){
    this.isMsg = false;
  }
  onFileSelected($event){}
}