import { Component, OnInit } from '@angular/core';
import { InvestInterest } from 'app/customers/InvestInterest';
import { FormControl } from '@angular/forms';
import { AdminDataService } from '../admin-data.service';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { BnNgIdleService } from 'bn-ng-idle';

@Component({
  selector: 'app-invest-interest-request',
  templateUrl: './invest-interest-request.component.html',
  styleUrls: ['./invest-interest-request.component.scss']
})
export class InvestInterestRequestComponent implements OnInit {
  invests : InvestInterest[] = [];
  date = new FormControl(new Date());
  serializedDate = new FormControl();
  page = 1;
  pageSize = 3;
  msg;
  isMsg = false;
  model;
  constructor(private adminService : AdminDataService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService
    ) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }
  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
      this.loadPenddingInvestRequest();
        }
        else{
          this.router.navigate(['/panel']);
        }
  }
  loadPenddingInvestRequest(){
    this.adminService.getPenddingInvestRequests().pipe(first()).subscribe(
      (invests) => { 
        this.invests = invests;
      },
      (error) => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  approveInvest(id: number){
    if(confirm('هل أنت متأكد من أنك تريد الموافقة على طلب تشغيل الأرباح')){
      this.adminService.approveInvestRequest(id).pipe(first()).subscribe(
        (res) =>{
          this.msg = res;
          this.isMsg = true;
        },
        (error) => {console.log('server error')},
        () => {console.log('completed...')}
      );
    }
  }
  close(){
    this.isMsg = false;
  }
}