import { Component, OnInit, Input } from '@angular/core';
import { User } from '../User';
import { AdminDataService } from '../admin-data.service';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-admin-display-employees',
  templateUrl: './admin-display-employees.component.html',
  styleUrls: ['./admin-display-employees.component.scss']
})
export class AdminDisplayEmployeesComponent implements OnInit {
  @Input() employees : User[] = [];
  message : string = 'تمت إضافة مستخدم جديد إلى النظام بنجاح!';
  page = 1;
  pageSize = 3;
  msg : string;
  isMsg : boolean = false;
  constructor(private adminService : AdminDataService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }

  ngOnInit() {
  }
  activateUser(id : number){
    if(confirm('هل أنت متأكد من أنك تريد تفعيل المستخدم ؟')){
      this.adminService.activateUser(id).pipe(first()).subscribe(
        (res) => {
          if(res !== null || res !== ''){
            console.log('تم التفعيل')
            this.msg = 'تم تفعيل المستخدم';
            this.isMsg = true;
          }
          else{
            console.log('no value present in response , that means it is null');
          }
        },
        (error) => {console.log('server error')},
        () => {console.log('completed...')}
      );
    }
  }
  deActivateUser(id : number){
    if(confirm('هل أنت متأكد من أنك تريد إلغاء تفعيل المستخدم؟')){
      this.adminService.deActivateUser(id).pipe(first()).subscribe(
        (res) => {
          if(res !== null || res !== ''){
            this.msg = 'تم إلغاء تغعيل المستخدم';
            this.isMsg = true;
          }
          else{
            console.log('no value present in the response that means it is null');
          }
        },
        (error) => {console.log('server error')},
        () => {console.log('completed')}
      );
    }
  }

}
