import { Component, OnInit } from '@angular/core';
import { Customer } from 'app/customers/Customer';
import { AdminDataService } from '../admin-data.service';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { AppSettings } from 'app/app-settings';
import { BnNgIdleService } from 'bn-ng-idle';

@Component({
  selector: 'app-admin-new-customers',
  templateUrl: './admin-new-customers.component.html',
  styleUrls: ['./admin-new-customers.component.scss']
})
export class AdminNewCustomersComponent implements OnInit {
  customers : Customer[] = [];
  customer : Customer = new Customer();
  page = 1;
  pageSize = 3;
  msg : string;
  isMsg = false;
  image : any;
  form : FormGroup;
  selectedFile : File;
  isShow: boolean = false;
  custImage: any = './assets/img/faces/default.png';
  custDelegImage: any = './assets/img/faces/default.png';
  custPassportImage: any = './assets/img/faces/default.png';
  constructor(private adminService : AdminDataService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService) {
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
     }
  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
      this.loadPenddinRegCustomer();
    }
    else{
      this.router.navigate(['/panel']);
    }
  }
  displayCustomerDetails(cust : Customer){
    this.customer = cust;
    //this.custImage = AppSettings.APP_ENDPOINT + '/customer/image/' + cust.id;
    this.custImage = cust.image;
    //this.custDelegImage = AppSettings.APP_ENDPOINT + '/customer/delegImage/' + cust.id;
    this.custDelegImage=cust.delegImage;
    //this.custPassportImage =AppSettings.APP_ENDPOINT+'/customer/custPassportImage/'+ cust.id;
    this.custPassportImage=cust.custPassImage;
    this.isShow = true;
  }
  activateCustomer(id : number){
    if(confirm('هل أنت متأكد من أنك تريد تفعيل المستثمر ؟')){
      this.adminService.activateCustomer(id).pipe(first()).subscribe(
        (msg) => {
          this.msg = msg;
          this.isMsg = true;
          this.loadPenddinRegCustomer();
          this.isShow = false;
        },
        (error) => {
          console.log('server error')
        }
        ,
        () => {console.log('completed...')}
        );
    }
  }
  loadPenddinRegCustomer(){
    this.adminService.getPenddingCustomers().pipe(first()).subscribe(
      (cust : Customer[])=> {
        if(cust !== null || cust.length !== 0){
          this.customers = cust
        }
      }
      );
  }
  close(){
    this.isMsg = false;
  }
}