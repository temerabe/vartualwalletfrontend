import { Component, OnInit } from '@angular/core';
import { AdminDataService } from '../admin-data.service';
import { Deposit } from 'app/customers/Deposit';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'app/authentication.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Memoize } from 'lodash-decorators';
import { BnNgIdleService } from 'bn-ng-idle';
declare const $: any;
@Component({
  selector: 'app-admin-deposit-requests',
  templateUrl: './admin-deposit-requests.component.html',
  styleUrls: ['./admin-deposit-requests.component.scss']
})
export class AdminDepositRequestsComponent implements OnInit {
  deposits: Deposit[] = [];
  page = 1;
  pageSize = 3;
  msg: string;
  isMsg: boolean = false;
  serializedDate = new FormControl();
  constructor(private adminService: AdminDataService,
    private auth: AuthenticationService,
    private router: Router,
    private bnIdle: BnNgIdleService
  ) { 
    this.bnIdle.startWatching(1800).subscribe((res) => {
      if(res) {
          console.log("session expired");
          this.auth.logOut();
          this.router.navigate(['/login']);
      }
    })
  }

  ngOnInit() {
    if (this.auth.isUserLoggedIn()) {
      this.loadPenddingDeposits();
    }
    else {
      this.router.navigate(['/panel']);
    }
  }
  loadPenddingDeposits() {
    this.adminService.getPenddinDpRequests().pipe(first()).subscribe(
      (dp) => {
        if (dp !== null) {
          this.deposits = dp
        } else {
          console.log('no pending deposit request to withdraw ');
        }
      },
      (err) => { },
      () => { }
    );
  }
  @Memoize
  getTxStatus(value){
    console.log('the status is = ' +value.status);
    let txStatus = '';
    if(value.status === 'pendding'){
      txStatus = 'إنتظار';
    }
    else if(value.status === 'reviewed'){
      txStatus = 'تمت المراحغة';
    }
    else if(value.status === 'canceled' ){
      txStatus = 'ملغية';
    }
    else {
      txStatus = 'تم السحب';
    }
    return txStatus;
  }
  approveDeposit(id: number) {
    const da: Date = this.serializedDate.value;
    if (da === null || da === undefined) {
      this.msg = 'الرجاء تحديد تاريخ الإيداع';
      this.isMsg = true;
    }
    else {
      const date : string = da.getFullYear()+"/"+(da.getMonth()+1)+"/"+da.getDate();
      if(confirm('هل انت متأكد من أنك توافق على عملية الإيداع ؟')){
      this.adminService.approveDeposit(id,date).pipe(first()).subscribe(
        msg => {
          this.msg = msg;
          this.isMsg = true;
        },
        (error) => {
          this.msg = 'فشل الإتصال بالسيرفر ، الرجاء المحاولة مرة أخرى';
          this.isMsg = true;
        },
        () => { console.log('completed...') }
      );
    }
  }
  }
  close() {
    this.isMsg = false;
  }
}
