import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AdminDataService } from '../admin-data.service';
import { User } from '../User';
import { UserserviceService } from 'app/userservices/userservice.service';
import { first } from 'rxjs/operators';
import {FormControl, FormGroupDirective,Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
declare const $:any;
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-admin-employees',
  templateUrl: './admin-employees.component.html',
  styleUrls: ['./admin-employees.component.scss']
})
export class AdminEmployeesComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();
  employees : User[] = [];
  message : string = 'تمت إضافة مستخدم جديد إلى النظام بنجاح!';
  page = 1;
  pageSize = 3;
  msg : string;
  isMsg : boolean = false;
  constructor(private adminService : AdminDataService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService
    ) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }

  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
    this.loadUser();
    }
    else{
      this.router.navigate(['/panel']);
    }
  }
  onSubmit(user : NgForm){
    this.adminService.createUser({
      firstName : user.value.firstName,
      lastName : user.value.lastName,
      email : user.value.email,
      password : user.value.password,
      phoneNo : user.value.phone,
    }).subscribe(
      (data : User) => {
        this.loadUser();
        this.msg = 'تمت الإضافة بنجاح';
        this.isMsg = true;
      }
    );
    }
    private loadUser(){
      const user : any = JSON.parse(localStorage.getItem('user'));
    this.adminService.getUsers().pipe(first()).subscribe(
      emps => {this.employees = emps}
      );
    }
  close(){
    this.isMsg = false;
  }
}