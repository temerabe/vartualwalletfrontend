import { Component, OnInit } from '@angular/core';
import { interestRateDeatils } from 'app/customers/InterestRateDetails';
import { AdminDataService } from '../admin-data.service';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { InterestReport } from '../InterestReport';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-year-reports',
  templateUrl: './year-reports.component.html',
  styleUrls: ['./year-reports.component.scss']
})
export class YearReportsComponent implements OnInit {
  page = 1;
  pageSize = 3;
  currentYearInterest: any = 0 ;
  allInterestCount: any = 0;
  interests: InterestReport[];
  constructor(
    private adminService: AdminDataService,
    private auth: AuthenticationService,
    private router: Router,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.getYearInterest();
    this.LoadAllInterestCount();
    this.LoadCurrentYearInterest();
  }
  LoadAllInterestCount(){
    var convertedDate = this.datePipe.transform(new Date(),'yyyy-MM-dd');
    this.adminService.getAllInterestCount().pipe(first()).subscribe(
      (res) => {
        this.allInterestCount = res;
      },
      (error) => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  getYearInterest(){
    var convertedDate = this.datePipe.transform(new Date(),'yyyy-MM-dd');
    this.adminService.getYearReport(convertedDate).pipe(first()).subscribe(
      (res) => {
        this.interests = res;
      },
      (error) =>{console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  LoadCurrentYearInterest(){
    var convertedDate = this.datePipe.transform(new Date(),'yyyy-MM-dd');
    this.adminService.getCurrentYearInterest(convertedDate).pipe(first()).subscribe(
      (res) => {
        this.currentYearInterest = res;
      },
      (error) => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
}