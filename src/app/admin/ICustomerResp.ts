export interface ICustomerResp {
	 id : number;
	firstName : string;
	middleName : string;
	lastName : string;
	password : string;
	email : string;
	accountNo : number;
	Image : string;
	accountStatus : string;
	passportNo: number;
	phoneNo : number;
	delegFullName : string;
	role : string;
	delegPassport : number;
	delegImage : string;
	totalBalance : number;
	availableBalance : number;
	balanceToWithdraw  : number;
	suspendedBalance : number;
}