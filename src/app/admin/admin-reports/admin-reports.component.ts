import { Component, OnInit } from '@angular/core';
import { AdminDataService } from '../admin-data.service';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
import { Customer } from 'app/customers/Customer';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-admin-reports',
  templateUrl: './admin-reports.component.html',
  styleUrls: ['./admin-reports.component.scss']
})
export class AdminReportsComponent implements OnInit {
  value: any = '';
  customers: Customer[] = [];
  msg:string = '';
  isMsg: boolean = false;
  constructor(
    private adminService: AdminDataService,
    private auth: AuthenticationService,
    private router: Router,
    private bnIdle: BnNgIdleService
  ) { }

  ngOnInit() {
    if (this.auth.isUserLoggedIn()) {
      this.loadCustomers();
    }
    else {
      this.router.navigate(['/panel']);
    }
  }
  loadCustomers() {
    this.adminService.getActiveCustomer().pipe(first()).subscribe(
      cust => { this.customers = cust }
    );
  }
  customerReposrt(customer,value){
    if(customer && value){
    this.router.navigate(['/admin-profits/reports/customer'],{queryParams:{account:customer,type:value}});
    }
    else{
      this.msg = 'الرجاء إختيار الخيارات المناسبة';
      this.isMsg = true;
    }
  }
  yearReposrt(){
    this.router.navigate(['/admin-profits/reports/year']);
  }
  monthReposrt(){
    this.router.navigate(['/admin-profits/reports/month']);
  }
  close(){
    this.isMsg = false;
  }
  filterMyOptions($event){}
}