import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './User';
import { Customer } from 'app/customers/Customer';
import { WithdrawRequest } from 'app/customers/WithdrawRequest';
import { TransferRequest } from 'app/customers/TransferRequest';
import { Deposit } from 'app/customers/Deposit';
import { AppSettings } from 'app/app-settings';
import { InvestInterest } from 'app/customers/InvestInterest';
import { interestRateDeatils } from 'app/customers/InterestRateDetails';
import { InterestReport } from './InterestReport';
@Injectable({
  providedIn: 'root'
})
export class AdminDataService {
  private crUurl = AppSettings.APP_ENDPOINT+'/users';
  constructor(private http : HttpClient) { }
  getPenddingCustomers() : Observable<Customer[]> {
    return this.http.get<Customer[]>(AppSettings.APP_ENDPOINT+'/admin/reg/status');
  }

  createUser(user : any) : Observable<User>{
    return this.http.post<User>(AppSettings.APP_ENDPOINT+'/users',user);
  }
  activateUser(id : number){
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/users/activate/'+id,null,{responseType : 'text'});
  }
  deActivateUser(id : number){
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/users/deactivate/'+id,null,{responseType : 'text'});
  }
  activateCustomer(id : number){
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/customers/activate/'+id,null,{responseType : 'text'});
  }
  deActivateCustomer(id : number){
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/customers/deactivate/'+id,null,{responseType : 'text'});
  }
  addBalance(balance : any){
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/customers/addbalance',balance,{responseType : 'text'});
  }
  addCommonInterest(interestRate : any,date: any){
    console.log('date = ' + date);
    const fd : FormData = new FormData();
    fd.append('interestRate',interestRate);
    fd.append('date',date);
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/customers/interest',fd,{responseType : 'text'});
  }
  getAddedInterests() : Observable<interestRateDeatils[]>{
    return this.http.get<interestRateDeatils[]>(AppSettings.APP_ENDPOINT+'/customer/tmp/interest');
  }
  addInterestPlus(interestRate : any,accountNo : any){
    const fd : FormData = new FormData();
    fd.append('interestRate',interestRate);
    fd.append('accountNo',accountNo);
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/customers/interestPlus',fd,{responseType : 'text'});
  }
  confirmInterest(id : any) {
    const fd : FormData = new FormData();
    fd.append('id', id);
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/confirm/interest',fd,{responseType : 'text'});
  }
  approveDeposit(id : number,date: string){
    const fd : FormData = new FormData();
    fd.append('date',date);
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/dp/approve/'+id,fd,{responseType : 'text'});
  }
  approveTransfer(id : number){
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/tr/approve/'+id,null,{responseType : 'text'});
  }
  approveWithdraw(id : number,date : string){
    const fd : FormData = new FormData();
    fd.append('date',date);
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/wr/approve/'+id,fd,{responseType : 'text'});
  }
  approveInvestRequest(id:number){
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/invest/approve/'+id,null,{responseType : 'text'});
  }
  checkLogin(email,password) : Observable<User>{
    console.log('email = ' + email + " -> password = "+ password);
    const url = AppSettings.APP_ENDPOINT+'/users/login';
    return this.http.post<User>(url,{
      email : email,
    	password : password
    });
  }
  updateCustomer(cust: Customer){
    return this.http.put(AppSettings.APP_ENDPOINT+'/admin/customers/update',cust,{responseType: 'text'});
  }
  getActiveCustomer() : Observable<Customer[]>{
    return this.http.get<Customer[]>(AppSettings.APP_ENDPOINT+'/admin/customers/active');
  }
  getUsers() : Observable<User[]>{
    return this.http.get<User[]>(AppSettings.APP_ENDPOINT+'/users');
  }
  getCustomers(){
    return this.http.get<Customer[]>(AppSettings.APP_ENDPOINT+'/admin/customers');
  }
  getPenddingWrRequests() : Observable<WithdrawRequest[]>{
    return this.http.get<WithdrawRequest[]>(AppSettings.APP_ENDPOINT+'/admin/wr/status');
  }
  getPenddingInvestRequests() : Observable<InvestInterest[]> {
    return this.http.get<InvestInterest[]>(AppSettings.APP_ENDPOINT+'/admin/invest/status');
  }
  getPenddingTrRequests() : Observable<TransferRequest[]>{
    return this.http.get<TransferRequest[]>(AppSettings.APP_ENDPOINT+'/admin/tr/status');
  }
  getPenddinDpRequests() : Observable<Deposit[]>{
    return this.http.get<Deposit[]>(AppSettings.APP_ENDPOINT+'/admin/dp/status');
  }
  getInActiveCustomersCount() : Observable<Number>{
    return this.http.get<Number>(AppSettings.APP_ENDPOINT+'/customers/inactive/count');
  }
  getPenddingWithdrawCount() : Observable<Number>{
    return this.http.get<Number>(AppSettings.APP_ENDPOINT+'/customers/penddingwithdraws/count');
  }
  getPenddingTransferCount() : Observable<Number>{
    return this.http.get<Number>(AppSettings.APP_ENDPOINT+'/customers/penddingtransfer/count');
  }
  getPenddingDepositCount() : Observable<Number>{
    return this.http.get<Number>(AppSettings.APP_ENDPOINT+'/customers/penddingdeposit/count');
  }
  getPenddingBalanceToToWithdraw() : Observable<Number>{
    return this.http.get<Number>(AppSettings.APP_ENDPOINT+'/customers/penddingwithdrawbalance/count');
  }
  getLastTransferCount() : Observable<Number>{
    return this.http.get<Number>(AppSettings.APP_ENDPOINT+'/customers/lasttransfer/count');
  }
  getLastWithdrawCount() : Observable<Number>{
    return this.http.get<Number>(AppSettings.APP_ENDPOINT+'/customers/lastwithdraw/count');
  }
  getCurrentMonthInterest(month: any){
    return this.http.get(AppSettings.APP_ENDPOINT+'/admin/interest/count/m/'+month);
  }
  getCurrentYearInterest(year: any){
    return this.http.get(AppSettings.APP_ENDPOINT+'/admin/interest/count/y/'+year);
  }
  getAllInterestCount(){
    return this.http.get(AppSettings.APP_ENDPOINT+'/admin/interest/all/count');
  }
  getMonthReport(month: any) : Observable<InterestReport[]>{
    return this.http.get<InterestReport[]>(AppSettings.APP_ENDPOINT+'/admin/interest/month/'+month);
  }
  getYearReport(year: any) : Observable<InterestReport[]>{
    return this.http.get<InterestReport[]>(AppSettings.APP_ENDPOINT+'/admin/interest/year/'+year);
  }
  getCustomerInterest(account : number,type:string): Observable<InterestReport[]>{
    return this.http.get<InterestReport[]>(AppSettings.APP_ENDPOINT+'/admin/interest/customer/'+account+'/and/'+type);
  }
  getCustomerMonthInterestCount(account:any) : Observable<InterestReport[]>{
    return this.http.get<InterestReport[]>(AppSettings.APP_ENDPOINT+'/admin/interest/customer/m/count/'+account);
  }
  getCustomerYearInterestCount(account:any) : Observable<InterestReport[]>{
    return this.http.get<InterestReport[]>(AppSettings.APP_ENDPOINT+'/admin/interest/customer/y/count/'+account);
  }
  getCustomerTotalInterestCount(account:any) : Observable<InterestReport[]>{
    return this.http.get<InterestReport[]>(AppSettings.APP_ENDPOINT+'/admin/interest/customer/all/count/'+account);
  }
}