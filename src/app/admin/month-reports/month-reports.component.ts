import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'app/authentication.service';
import { AdminDataService } from '../admin-data.service';
import { DatePipe } from '@angular/common';
import { first } from 'rxjs/operators';
import { interestRateDeatils } from 'app/customers/InterestRateDetails';
import { InterestReport } from '../InterestReport';

@Component({
  selector: 'app-month-reports',
  templateUrl: './month-reports.component.html',
  styleUrls: ['./month-reports.component.scss']
})
export class MonthReportsComponent implements OnInit {
  currentMonthInterest: any = 0 ;
  currentYearInterest: any = 0 ;
  allInterestCount: any = 0;
  page = 1;
  pageSize = 3;
  interests: InterestReport[];
  constructor(
    private adminService: AdminDataService,
    private auth: AuthenticationService,
    private router: Router,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.LoadCurrentMonthInterest();
    this.LoadAllInterestCount();
    this.getMonthInterest();
  }
  LoadCurrentMonthInterest(){
    var convertedDate = this.datePipe.transform(new Date(),'yyyy-MM-dd');
    this.adminService.getCurrentMonthInterest(convertedDate).pipe(first()).subscribe(
      (res) => {
        this.currentMonthInterest = res;
      },
      (error) => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  LoadAllInterestCount(){
    var convertedDate = this.datePipe.transform(new Date(),'yyyy-MM-dd');
    this.adminService.getAllInterestCount().pipe(first()).subscribe(
      (res) => {
        this.allInterestCount = res;
      },
      (error) => {console.log('server error')},
      () => {console.log('completed...')}
    );
  }
  getMonthInterest(){
    var convertedDate = this.datePipe.transform(new Date(),'yyyy-MM-dd');
    this.adminService.getMonthReport(convertedDate).pipe(first()).subscribe(
      (res) => {
        this.interests = res;
      },
      (error) =>{console.log('server error')},
      () => {console.log('completed...')}
    );
  }
}