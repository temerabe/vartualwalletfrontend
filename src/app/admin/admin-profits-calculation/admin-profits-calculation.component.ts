import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { AdminDataService } from '../admin-data.service';
import { Customer } from 'app/customers/Customer';
import { NgForm, FormControl } from '@angular/forms';
import { AuthenticationService } from 'app/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { interestRateDeatils } from 'app/customers/InterestRateDetails';
import { BnNgIdleService } from 'bn-ng-idle';
import { DatePipe } from '@angular/common';
declare const $: any;
@Component({
  selector: 'app-admin-profits-calculation',
  templateUrl: './admin-profits-calculation.component.html',
  styleUrls: ['./admin-profits-calculation.component.scss']
})
export class AdminProfitsCalculationComponent implements OnInit {
  customers: Customer[] = [];
  interests: interestRateDeatils[] = [];
  page = 1;
  pageSize = 3;
  msg1: string = 'تمت إضافة نسبة الأرباح للمستثمرين بنجاح';
  msg2: string = 'تمت إضافة نسبة الأرباح للمستثمر بنجاح';
  isMsg1: boolean = false;
  isMsg2: boolean = false;
  disable: boolean = true;
  value: any = '';
  serializedDate = new FormControl(new Date());
  constructor(private adminService: AdminDataService,
    private auth: AuthenticationService,
    private router: Router,
    private bnIdle: BnNgIdleService,
    private datePipe: DatePipe,
    private route: ActivatedRoute
  ) {
    this.bnIdle.startWatching(1800).subscribe((res) => {
      if(res) {
          console.log("session expired");
          this.auth.logOut();
          this.router.navigate(['/login']);
      }
    })
  }

  ngOnInit() {
    if (this.auth.isUserLoggedIn()) {
      this.loadCustomers();
      this.loadTemporaryAddedInterest();
    }
    else {
      this.router.navigate(['/panel']);
    }
  }
  loadCustomers() {
    this.adminService.getCustomers().pipe(first()).subscribe(
      cust => {
        this.customers = cust;
      }
    );
  }
  loadTemporaryAddedInterest() {
    this.adminService.getAddedInterests().pipe(first()).subscribe(
      (res) => {
        this.interests = res;
      },
      (error) => { console.log('server error') },
      () => { console.log('completed...') }
    );
  }
  addCommonInterest(f: NgForm) {
    const da: Date = this.serializedDate.value;
    //var convertedDate = da.toISOString();
    var convertedDate = this.datePipe.transform(da,'yyyy-MM-dd');
  // convertedDate = convertedDate.replace('at', '');
   // var date = new Date(convertedDate);
  var date = convertedDate;
    console.log('date = ' + date);
    if (confirm('هل أنت متأكد من أنك تريد إضافة نسبة الأرباح العامة ؟')) {
      this.adminService.addCommonInterest(f.value.interestRate,date).pipe(first()).subscribe(
        (msg) => {
          console.log('message = ' + msg);
          this.loadTemporaryAddedInterest();
          this.msg1 = msg;
          this.isMsg1 = true;
        },
        (error) => {
          console.log(error);
          this.msg1  = 'فشل الإتصال بالسيرفر ، حاول مرة أخرى';
          this.isMsg1 = true;
        },
        () => { console.log('completed...') }
      );
    }

  }
  addInterestPlus(f: NgForm) {
    if (confirm('هل أنت متأكد من أنك تريد إضافة نسبة أرباح خاصة لهذا المستثمر ؟')) {
      this.adminService.addInterestPlus(f.value.interestRate, f.value.accountNo).pipe(first()).subscribe(
        (msg) => {
          console.log('message = ' + msg);
          this.loadTemporaryAddedInterest();
          this.msg2 = msg;
          this.isMsg2 = true;
        },
        (error) => {
          this.msg2 = 'فشل الإتصال بالسيرفر ، الرجاء المحاولة مرة أخرى';
          this.isMsg2 = true;
        },
        () => { console.log('cmopleted...') }
      );
    }
  }
  close1() {
    this.isMsg1 = false; 
  }
  close2() {
    this.isMsg2 = false;
  }
  filterMyOptions(event) {

  }
  loadInterest(event) {
    this.loadTemporaryAddedInterest();
  }
  interestReports(){
    
  }
  reports(){
    this.disable = false;
    this.router.navigate(['/admin-profits/reports']);
  }
}