export class InterestReport {
	public id:number;
	public accountNo:number;
	public fullName:string;
	public interestRate:number;
	public customerBalance:number;
	public customerAvailableBalance:number;
	public customerInterestBalance:number;
	public month:any;
}