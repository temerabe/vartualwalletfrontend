export interface User {
    id : number;
	firstName : string;
	lastName : string;
	email : string;
	password : string;
	phoneNo : number;
	role : string;
	status : string;
	createdDate : any;
}