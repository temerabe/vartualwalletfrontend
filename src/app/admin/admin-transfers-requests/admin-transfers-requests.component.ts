import { Component, OnInit, Input } from '@angular/core';
import {first} from 'rxjs/operators';
import { AdminDataService } from '../admin-data.service';
import { TransferRequest } from 'app/customers/TransferRequest';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { Memoize } from 'lodash-decorators';
import { BnNgIdleService } from 'bn-ng-idle';
declare const $:any;
@Component({
  selector: 'app-admin-transfers-requests',
  templateUrl: './admin-transfers-requests.component.html',
  styleUrls: ['./admin-transfers-requests.component.scss']
})
export class AdminTransfersRequestsComponent implements OnInit {
  transfers : TransferRequest[] = [];
  page = 1;
  pageSize = 3;
  msg : string;
  isMsg : boolean = false;
  constructor(private adminService : AdminDataService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService
    ) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }

  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
    this.loadPenddingRequest();
    }
    else{
      this.router.navigate(['/panel']);
    }
  }
  loadPenddingRequest(){
    this.adminService.getPenddingTrRequests().pipe(first()).subscribe(
      (tr) => {
        if(tr !== null ){
        this.transfers = tr;
        console.log('account ' + tr[0].accountNo);
        }
        else{console.log('no pending transfer request to approve')}
      },
      (err) => {console.log('server error')},
      () => {console.log('comepleted...')}
    );
  }
  approveTransfer(id : number){
    if(confirm('هل أنت متأكد من أنك تريد الموافقة على عملية التحويل ؟')){
      this.adminService.approveTransfer(id).pipe(first()).subscribe(
        (msg) => {
          this.msg = msg;
          this.isMsg = true;
        },
        (error) => {
          this.msg = 'فشل الإتصال بالسيرفر';
          this.isMsg = false;
        },
        () => {console.log('completed')}
      );
    }
  }
  @Memoize
  getTxStatus(value){
    console.log('the status is = ' +value.status);
    let txStatus = '';
    if(value.status === 'pendding'){
      txStatus = 'إنتظار';
    }
    else if(value.status === 'reviewed'){
      txStatus = 'تمت المراحغة';
    }
    else if(value.status === 'canceled' ){
      txStatus = 'ملغية';
    }
    else {
      txStatus = 'تم السحب';
    }
    return txStatus;
  }
 close(){
   this.isMsg = false;
 }
}