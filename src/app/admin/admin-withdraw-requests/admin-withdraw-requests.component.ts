import { Component, OnInit } from '@angular/core';
import { AdminDataService } from '../admin-data.service';
import { first } from 'rxjs/operators';
import { WithdrawRequest } from 'app/customers/WithdrawRequest';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { BnNgIdleService } from 'bn-ng-idle';
declare const $:any;
@Component({
  selector: 'app-admin-withdraw-requests',
  templateUrl: './admin-withdraw-requests.component.html',
  styleUrls: ['./admin-withdraw-requests.component.scss']
})
export class AdminWithdrawRequestsComponent implements OnInit {
  withdraws : WithdrawRequest[] = [];
  date = new FormControl(new Date());
  serializedDate = new FormControl();
  page = 1;
  pageSize = 3;
  msg;
  isMsg = false;
  model;
  constructor(private adminService : AdminDataService,
    private auth : AuthenticationService,
    private router : Router,
    private bnIdle: BnNgIdleService
    ) { 
      this.bnIdle.startWatching(1800).subscribe((res) => {
        if(res) {
            console.log("session expired");
            this.auth.logOut();
            this.router.navigate(['/login']);
        }
      })
    }

  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
  this.loadPenddingWrRequest();
    }
    else{
      this.router.navigate(['/panel']);
    }
  }
  loadPenddingWrRequest(){
    this.adminService.getPenddingWrRequests().pipe(first()).subscribe(
      wr => { 
        this.withdraws = wr;
      }
    );
  }
  approveWithdraw(id : number){
    const da  : Date = this.serializedDate.value;
    if(da === null || da === undefined ){
      this.msg = 'الرجاء تحديد تاريخ السحب';
      this.isMsg = true;
    }
    else{
      const date : string = da.getFullYear()+"/"+(da.getMonth()+1)+"/"+da.getDate();
      if(confirm('هل انت متأكد من أنك توافق على عملية السحب ؟')){
          this.adminService.approveWithdraw(id,date).pipe(first()).subscribe(
            msg => {
              if(msg !== null || msg === ''){
              this.isMsg = true;
              this.msg = msg;
              console.log('msg = ' + msg);
              }
              else{
                console.log('no pendding withdraws to approve');
              }
            },
            (err) => { 
              this.msg = 'فشل الإتصال بالسيرفر ، الرجاء المحاولة مرة أخرى';
              this.isMsg = true;
            },
            () => { console.log('compeleted...')}
          );
        }
    }
  }
  close(){
    this.isMsg = false;
  }
}
