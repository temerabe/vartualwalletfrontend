import { Component, OnInit } from '@angular/core';
import { AdminDataService } from '../admin-data.service';
import { Customer } from 'app/customers/Customer';
import { first } from 'rxjs/operators';
import { NgForm, FormControl } from '@angular/forms';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
declare const $: any;
@Component({
  selector: 'app-add-balance',
  templateUrl: './add-balance.component.html',
  styleUrls: ['./add-balance.component.scss']
})
export class AddBalanceComponent implements OnInit {
  customers: Customer[] = [];
  msg: string;
  isMsg: boolean = false;
  serializedDate = new FormControl(new Date());
  value: any = '';
  constructor(private adminService: AdminDataService,
    private auth: AuthenticationService,
    private router: Router,
    private bnIdle: BnNgIdleService
  ) {
    this.bnIdle.startWatching(1800).subscribe((res) => {
      if(res) {
          console.log("session expired");
          this.auth.logOut();
          this.router.navigate(['/login']);
      }
    })
  }

  ngOnInit() {
    if (this.auth.isUserLoggedIn()) {
      this.loadCustomers();
    }
    else {
      this.router.navigate(['/panel']);
    }
  }
  loadCustomers() {
    this.adminService.getActiveCustomer().pipe(first()).subscribe(
      cust => { this.customers = cust }
    );
  }
  onSubmit(f: NgForm) {
    const da: Date = this.serializedDate.value;
    var convertedDate = da.toLocaleDateString();
    convertedDate = convertedDate.replace('at', '');
    var date = new Date(convertedDate);
    var woDays;
    var isFromBegining;
    if (date.getDate() === 1) {
      woDays = 30;
      isFromBegining = true;
    }
    else {
      woDays = 30 - date.getDate();
      isFromBegining = false;
    }
    console.log('is from begining = ' + isFromBegining);
    console.log('the day is = ' + woDays);
    if (confirm('هل أنت متأكد من أنك تريد إضافة رصيد لهذا المستثمر ؟')) {
      this.adminService.addBalance({
        accountNo: f.value.accountNo,
        amount: f.value.amount,
        startedAt: date,
        proccessType: "admin",
        notes: f.value.notes,
        workingDays: woDays,
        isFromBegining
      }).pipe(first()).subscribe(
        msg => {
          this.msg = msg;
          this.isMsg = true;
        }
      );
    }
  }
  close() {
    this.isMsg = false;
  }
  //filterMyOptions(emps: Customer[], searchTerm: string): Customer[] {
    filterMyOptions($event) {  
  // if (!emps || !searchTerm) {
    //   return emps;
    // }
    // return emps.filter(employee => employee.firstName.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1
    // );
  }
}