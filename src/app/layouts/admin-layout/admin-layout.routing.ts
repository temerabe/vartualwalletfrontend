import { Routes } from '@angular/router';
import { CustomerDashboardComponent } from 'app/customers/customer-dashboard/customer-dashboard.component';
import { CustomerProfileComponent } from 'app/customers/customer-profile/customer-profile.component';
import { DepositeRequestComponent } from 'app/customers/deposite-request/deposite-request.component';
import { WithdrawRequestComponent } from 'app/customers/withdraw-request/withdraw-request.component';
import { TransferRequestComponent } from 'app/customers/transfer-request/transfer-request.component';
import { CustomerContactManagmentComponent } from 'app/customers/customer-contact-managment/customer-contact-managment.component';
import { AdminDashboardComponent } from 'app/admin/admin-dashboard/admin-dashboard.component';
import { AdminCustomerComponent } from 'app/admin/admin-customer/admin-customer.component';
import { AdminEmployeesComponent } from 'app/admin/admin-employees/admin-employees.component';
import { AdminWithdrawRequestsComponent } from 'app/admin/admin-withdraw-requests/admin-withdraw-requests.component';
import { AdminDepositRequestsComponent } from 'app/admin/admin-deposit-requests/admin-deposit-requests.component';
import { AdminTransfersRequestsComponent } from 'app/admin/admin-transfers-requests/admin-transfers-requests.component';
import { AdminProfitsCalculationComponent } from 'app/admin/admin-profits-calculation/admin-profits-calculation.component';
import { CustomerRequestsReportComponent } from 'app/customers/reports/customer-requests-report/customer-requests-report.component';
import { CustomerDetailsReportComponent } from 'app/customers/reports/customer-details-report/customer-details-report.component';
import { CustomerTransfersReportComponent } from 'app/customers/reports/customer-transfers-report/customer-transfers-report.component';
import { AddBalanceComponent } from 'app/admin/add-balance/add-balance.component';
import { EmployeeDashboardComponent } from 'app/employees/employee-dashboard/employee-dashboard.component';
import { EmployeeWithdrawRequestsComponent } from 'app/employees/employee-withdraw-requests/employee-withdraw-requests.component';
import { EmployeeDepositRequestsComponent } from 'app/employees/employee-deposit-requests/employee-deposit-requests.component';
import { InvestInterestRequestComponent } from 'app/admin/invest-interest-request/invest-interest-request.component';
import { CustomerDepositReportComponent } from 'app/customers/reports/customer-deposit-report/customer-deposit-report.component';
import { InterestInvestmentComponent } from 'app/customers/interest-investment/interest-investment.component';
import { AdminNewCustomersComponent } from 'app/admin/admin-new-customers/admin-new-customers.component';
import { AdminCustomerEditComponent } from 'app/admin/admin-customer-edit/admin-customer-edit.component';
import { AdminReportsComponent } from 'app/admin/admin-reports/admin-reports.component';
import { MonthReportsComponent } from 'app/admin/month-reports/month-reports.component';
import { YearReportsComponent } from 'app/admin/year-reports/year-reports.component';
import { CustomerReportsComponent } from 'app/admin/customer-reports/customer-reports.component';
export const CustomerLayoutRoutes: Routes = [
    { path: 'customer-dashboard',    component : CustomerDashboardComponent   },
    { path: 'customer-dashboard/:role',    component : CustomerDashboardComponent   },
    { path: 'customer-profile',   component: CustomerProfileComponent },
    { path: 'customer-deposite-request',     component: DepositeRequestComponent },
    { path: 'customer-withdraw-request',     component: WithdrawRequestComponent },
    { path: 'customer-transfer-request',          component: TransferRequestComponent },
    { path: 'customer-contact-managment',           component: CustomerContactManagmentComponent },
    { path: 'customer-requests-reports',           component: CustomerRequestsReportComponent },
    { path: 'customer-details-reports',           component: CustomerDetailsReportComponent },
    { path: 'customer-deposit-reports' , component : CustomerDepositReportComponent },
    { path: 'customer-transfer-reports',           component: CustomerTransfersReportComponent },
    { path: 'customer-interest-investment' , component: InterestInvestmentComponent},
];
export const AdminLayoutRoutes: Routes = [
    { path: 'admin-dashboard',    component : AdminDashboardComponent   },
    { path: 'admin-customers',   component: AdminCustomerComponent },
    { path: 'admin-employees',     component: AdminEmployeesComponent },
    { path: 'admin-profits' , component : AdminProfitsCalculationComponent,children:[
        {path: 'reports' , component: AdminReportsComponent,children:[
            {path: 'month' , component:MonthReportsComponent},
            {path:'year',component:YearReportsComponent},
            {path:'customer',component:CustomerReportsComponent}
        ]}
    ]},
    { path: 'admin-add-balance' , component : AddBalanceComponent },
    { path: 'admin-withdraw-requests',     component: AdminWithdrawRequestsComponent },
    { path: 'admin-transfer-requests',          component: AdminTransfersRequestsComponent },
    { path: 'admin-deposit-requests',          component: AdminDepositRequestsComponent },
    { path: 'admin-invest-interest' , component : InvestInterestRequestComponent },
    { path: 'admin-new-customers' , component : AdminNewCustomersComponent},
    { path: 'admin-customer-edit' , component : AdminCustomerEditComponent}
];
export const EmployeeLayoutRoutes: Routes = [
    { path: 'employee-dashboard',    component : EmployeeDashboardComponent   },
    { path: 'employee-withdraw-requests' , component : EmployeeWithdrawRequestsComponent },
    { path: 'employee-deposit-requests' , component : EmployeeDepositRequestsComponent}
];