import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes, CustomerLayoutRoutes, EmployeeLayoutRoutes } from './admin-layout.routing';
import { Debounce } from 'lodash-decorators';
import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';
import { CustomerDashboardComponent } from 'app/customers/customer-dashboard/customer-dashboard.component';
import { CustomerProfileComponent } from 'app/customers/customer-profile/customer-profile.component';
import { WithdrawRequestComponent } from 'app/customers/withdraw-request/withdraw-request.component';
import { DepositeRequestComponent } from 'app/customers/deposite-request/deposite-request.component';
import { TransferRequestComponent } from 'app/customers/transfer-request/transfer-request.component';
import { CustomerContactManagmentComponent } from 'app/customers/customer-contact-managment/customer-contact-managment.component';
import { CustomerRequestsReportComponent } from 'app/customers/reports/customer-requests-report/customer-requests-report.component';
import { CustomerTransfersReportComponent } from 'app/customers/reports/customer-transfers-report/customer-transfers-report.component';
import { CustomerDetailsReportComponent } from 'app/customers/reports/customer-details-report/customer-details-report.component';
import { AdminDashboardComponent } from 'app/admin/admin-dashboard/admin-dashboard.component';
import { AdminCustomerComponent } from 'app/admin/admin-customer/admin-customer.component';
import { AdminDepositRequestsComponent } from 'app/admin/admin-deposit-requests/admin-deposit-requests.component';
import { AdminWithdrawRequestsComponent } from 'app/admin/admin-withdraw-requests/admin-withdraw-requests.component';
import { AdminTransfersRequestsComponent } from 'app/admin/admin-transfers-requests/admin-transfers-requests.component';
import { AdminEmployeeRegistrationComponent } from 'app/admin/admin-employee-registration/admin-employee-registration.component';
import { AdminEmployeesComponent } from 'app/admin/admin-employees/admin-employees.component';
import { AdminProfitsCalculationComponent } from 'app/admin/admin-profits-calculation/admin-profits-calculation.component';
import { AddBalanceComponent } from 'app/admin/add-balance/add-balance.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { EmployeeDepositRequestsComponent } from 'app/employees/employee-deposit-requests/employee-deposit-requests.component';
import { EmployeeWithdrawRequestsComponent } from 'app/employees/employee-withdraw-requests/employee-withdraw-requests.component';
import { EmployeeDashboardComponent } from 'app/employees/employee-dashboard/employee-dashboard.component';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { InvestInterestRequestComponent } from 'app/admin/invest-interest-request/invest-interest-request.component';
import { CustomerDepositReportComponent } from 'app/customers/reports/customer-deposit-report/customer-deposit-report.component';
import { InterestInvestmentComponent } from 'app/customers/interest-investment/interest-investment.component';
import { AdminNewCustomersComponent } from 'app/admin/admin-new-customers/admin-new-customers.component';
import {MatTableModule} from '@angular/material/table';
import {MatGridListModule} from '@angular/material/grid-list';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import {SelectModule} from 'ng2-select';
import { EmployeeFilterPipe } from 'app/admin/employee-filter.pipe';
import { CustomerFilterPipe } from 'app/customer-pipes/customer-filter.pipe';
import { AdminGeneratedInterestComponent } from 'app/admin/admin-generated-interest/admin-generated-interest.component';
import { AdminDisplayEmployeesComponent } from 'app/admin/admin-display-employees/admin-display-employees.component';
import {MatStepperModule} from '@angular/material/stepper';
import { AdminCustomerEditComponent } from 'app/admin/admin-customer-edit/admin-customer-edit.component';
import { AdminReportsComponent } from 'app/admin/admin-reports/admin-reports.component';
import { MonthReportsComponent } from 'app/admin/month-reports/month-reports.component';
import { YearReportsComponent } from 'app/admin/year-reports/year-reports.component';
import { CustomerReportsComponent } from 'app/admin/customer-reports/customer-reports.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    RouterModule.forChild(CustomerLayoutRoutes),
    RouterModule.forChild(EmployeeLayoutRoutes),
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MaterialFileInputModule,
    MatFormFieldModule,
    MatInputModule,
    MDBBootstrapModule.forRoot(),
    MatSelectModule,
    MatTooltipModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatTableModule,
    MatGridListModule,
    NgxMatSelectSearchModule,
    SelectModule,
    MatStepperModule
  ],
  declarations: [
    CustomerDashboardComponent,
    CustomerProfileComponent,
    WithdrawRequestComponent,
    DepositeRequestComponent,
    TransferRequestComponent,
    CustomerContactManagmentComponent,
    CustomerRequestsReportComponent,
    CustomerTransfersReportComponent,
    CustomerDetailsReportComponent,
    AdminDashboardComponent,
    AdminCustomerComponent,
    AdminEmployeesComponent,
    AdminDepositRequestsComponent,
    AdminWithdrawRequestsComponent,
    AdminTransfersRequestsComponent,
    AdminEmployeeRegistrationComponent,
    AddBalanceComponent,
    AdminProfitsCalculationComponent,
    EmployeeWithdrawRequestsComponent,
    EmployeeDepositRequestsComponent,
    EmployeeDashboardComponent,
    InvestInterestRequestComponent,
    CustomerDepositReportComponent,
    InterestInvestmentComponent,
    AdminNewCustomersComponent,
    EmployeeFilterPipe,
    CustomerFilterPipe,
    AdminGeneratedInterestComponent,
    AdminDisplayEmployeesComponent,
    AdminCustomerEditComponent,
    AdminReportsComponent,
    MonthReportsComponent,
    YearReportsComponent,
    CustomerReportsComponent
  ],
  providers:[DatePipe]
})
export class AdminLayoutModule {}