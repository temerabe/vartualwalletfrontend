import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { HomeComponent } from './home/home.component';
import { CustomerLoginComponent } from './customers/customer-login/customer-login.component';
import { CustomerRegistrationComponent } from './customers/customer-registration/customer-registration.component';
import { CustomerForgotPasswordComponent } from './customers/customer-forgot-password/customer-forgot-password.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { CreateProfileComponent } from './customers/create-profile/create-profile.component';
import { ResetpasswordComponent } from './customers/resetpassword/resetpassword.component';
const routes: Routes =[
  { path: '' , redirectTo:'/login',pathMatch:'full'},
  { path: 'login' , component : CustomerLoginComponent ,children:[
    {path:':id' , component: CustomerLoginComponent}
  ]},
  { path: 'home' , component : HomeComponent},
  { path: 'forgot-password' , component: CustomerForgotPasswordComponent },
  { path: 'reset-password/:id' , component: ResetpasswordComponent },
  { path : 'panel' , component : UserLoginComponent },
  { path : 'create-profile/:id' , component : CreateProfileComponent },
  { path: 'register' , component : CustomerRegistrationComponent
  },
  { path: 'forgot-password' , component : CustomerForgotPasswordComponent},
   {  path: '', component: AdminLayoutComponent, children: [
     { path: '', loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule' }
    ]
  },
];
@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    // RouterModule.forRoot(routes,{
    //    useHash: true
    // })
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
