import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from 'app/customers/Customer';
import { User } from 'app/admin/User';
import { Transaction } from 'app/customers/Transaction';
import { AppSettings } from 'app/app-settings';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  constructor(private http : HttpClient) { }
  getCompleteTransaction(id : number) : Observable<Transaction>{
    console.log('trno = ' + id);
    return this.http.get<Transaction>(AppSettings.APP_ENDPOINT+'/employee/transaction/'+id);
  }
  getCustomer(accountno : number) : Observable<Customer>{
    return this.http.get<Customer>(AppSettings.APP_ENDPOINT+'/employee/transaction/'+accountno);
  }
  
  executeTransaction(trNo : any){
    console.log('tr no = ' + trNo);
    const fd : FormData = new FormData();
    fd.append('trNo', trNo );
    return this.http.put(AppSettings.APP_ENDPOINT+'/employee/ex/transaction',fd,{responseType : 'text'});
  }
}