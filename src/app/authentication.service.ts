import { Injectable } from '@angular/core';
import { CustomersService } from './customers/customers.service';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  role : string;
  setRole(role : string){
    this.role = role;
  }
  getRole(){
    return this.role;
  }
  constructor(private custServ : CustomersService) { }
  authenticate(username, password){
      this.custServ.checkLogin(username,password);
  }
  isUserLoggedIn() {
    // let user  : any = sessionStorage.getItem('user');
    let user : any = localStorage.getItem('user');
    console.log('is user === null  ? ' +  (user === null));
    return !(user === null)
  }
  logOut() {
    //sessionStorage.removeItem('user');
    localStorage.removeItem('user');
  }
}
