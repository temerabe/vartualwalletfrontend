import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'app/authentication.service';
declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const CUSTOMERSROUTES: RouteInfo[] = [
  { path: '/customer-dashboard', title: 'الرئيسية',  icon: 'home', class: '' },
  { path: '/customer-profile', title: 'الملف الشخصي',  icon:'person', class: '' },
  { path: '/customer-interest-investment' , title: 'طلب تشغيل الأرباح' , icon:'money_off' , class: ''},
  { path: '/customer-deposite-request', title: 'طلب إيداع',  icon:'person', class: '' },
  { path: '/customer-withdraw-request', title: 'طلب سحب',  icon:'money_off', class: '' },
  { path: '/customer-transfer-request', title: 'طلب تحويل',  icon:'library_books', class: '' },
  // { path: '/customer-reports', title: 'إدارة التقارير',  icon:'business', class: '' },
  { path: '/customer-requests-reports', title: 'سجل طلبات السحب',  icon:'contact_mail', class: '' },
  { path: '/customer-transfer-reports', title: 'سجل طلبات التحويل',  icon:'contact_mail', class: '' },
  { path: '/customer-deposit-reports' , title: 'سجل طلبات التحويل' , icon:'contact_mail' , class: ''},
  { path: '/customer-details-reports', title: 'كشف حساب مفصل',  icon:'contact_mail', class: '' },
  { path: '/login',title:'تسجيل الخروج',icon:'library_books',class:''}
  //{ path: '/customer-contact-managment', title: 'مراسلة الإدارة',  icon:'contact_mail', class: '' },
];
export const ADMINROUTES: RouteInfo[] = [
  { path: '/admin-dashboard', title: 'الرئيسية',  icon: 'home', class: '' },
  { path: '/admin-new-customers' , title: 'مستثمرين جدد', icon:'person', class:''},
  { path: '/admin-add-balance' , title: 'إضافة رصيد' , icon : 'person' , class : ''},
  { path: '/admin-customers', title: 'العملاء',  icon:'person', class: '' },
  { path: '/admin-employees', title: 'الموظفين',  icon:'person', class: '' },
  { path: '/admin-profits', title: 'حساب نسبة الأرباح',  icon:'library_books', class: '' },
  { path: '/admin-invest-interest' , title: 'طلبات تشغيل الأرباح' , icon:'library_books' , class: ''},
  { path: '/admin-deposit-requests', title: 'طلبات الإيداع',  icon:'library_books', class: '' },
  { path: '/admin-transfer-requests', title: 'طلبات التحويل',  icon:'money_off', class: '' },
  { path: '/admin-withdraw-requests', title: 'طلبات السحب',  icon:'library_books', class: '' },
  { path: '/panel',title:'تسجيل الخروج',icon:'library_books',class:''}
];
export const EMPLOYEES: RouteInfo[] = [
  { path: '/employee-dashboard', title: 'الرئيسية',  icon: 'home', class: '' },
  //{ path: '/employee-deposit-requests', title: 'طلبات الإيداع',  icon:'person', class: '' },
  //{ path: '/employee-withdraw-requests' , title: 'طلبات السحب' , icon : 'person' , class : ''}
  { path: '/login/panel',title:'تسجيل الخروج',icon:'library_books',class:''}
];
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  title = 'لوحة تحكم المستثمر';
  constructor(private route : ActivatedRoute,
    private auth : AuthenticationService,
    private router : Router
    ) { }
  ngOnInit() {
    if(this.auth.isUserLoggedIn()){
      const user : any = JSON.parse(localStorage.getItem('user'));
      if('customer' === user.role){
        this.title = 'لوحة تحكم المستثمر';
         this.menuItems = CUSTOMERSROUTES.filter(menuItem => menuItem);
      }
      else if('admin' === user.role){
        console.log('the role in admin = ' + user.role);
        this.title = 'لوحة تحكم المدير'
        this.menuItems = ADMINROUTES.filter(menuItem => menuItem);
      }
      else{
        this.title = 'لوحة تحكم الموظف'
        this.menuItems = EMPLOYEES.filter(menuItem => menuItem);
      }
    }
    else{
      this.router.navigate(['/login']);
    }
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
  logout(){
    this.auth.logOut();
    this.router.navigate(['']);
}
}