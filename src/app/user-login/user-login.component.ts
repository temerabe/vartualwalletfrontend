import { Component, OnInit } from '@angular/core';
import { AdminDataService } from 'app/admin/admin-data.service';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';
import { User } from 'app/admin/User';
import { AuthenticationService } from 'app/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {
  role : string = 'user';
  error : boolean = false;
  errorMessage : string = 'إسم المستخدم أو كلمة المرور غير صحيحين';
  constructor(
    private adminService : AdminDataService,
    private auth: AuthenticationService,
    private router : Router
    ) { }

  ngOnInit() {
    
  }
  checkLogin(form : NgForm){
    this.adminService.checkLogin(form.value.email,form.value.password).pipe(first()).subscribe(
      (user : User) => {
       if(user.role === 'admin'){
         localStorage.setItem('user' , JSON.stringify(user));
         this.router.navigate(['/admin-dashboard']);
       }
       else if(user.role === 'user' ){
        localStorage.setItem('user' , JSON.stringify(user));
         this.router.navigate(['/employee-dashboard']);
       }
       else{
        console.log('username or password does not exist');
       }
      },
      (error) => {
        this.error = true;
        console.log('server error');
      },
      () => {console.log('completed...')}
    );
  }
}
